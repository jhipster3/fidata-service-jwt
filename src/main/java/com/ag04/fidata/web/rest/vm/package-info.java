/**
 * View Models used by Spring MVC REST controllers.
 */
package com.ag04.fidata.web.rest.vm;
