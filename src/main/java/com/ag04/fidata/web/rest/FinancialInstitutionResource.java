package com.ag04.fidata.web.rest;

import com.ag04.fidata.domain.FinancialInstitution;
import com.ag04.fidata.service.FinancialInstitutionService;
import com.ag04.fidata.web.rest.errors.BadRequestAlertException;
import com.ag04.fidata.service.dto.FinancialInstitutionCriteria;
import com.ag04.fidata.service.FinancialInstitutionQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.ag04.fidata.domain.FinancialInstitution}.
 */
@RestController
@RequestMapping("/api")
public class FinancialInstitutionResource {

    private final Logger log = LoggerFactory.getLogger(FinancialInstitutionResource.class);

    private static final String ENTITY_NAME = "fidataServiceFinancialInstitution";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final FinancialInstitutionService financialInstitutionService;

    private final FinancialInstitutionQueryService financialInstitutionQueryService;

    public FinancialInstitutionResource(FinancialInstitutionService financialInstitutionService, FinancialInstitutionQueryService financialInstitutionQueryService) {
        this.financialInstitutionService = financialInstitutionService;
        this.financialInstitutionQueryService = financialInstitutionQueryService;
    }

    /**
     * {@code POST  /financial-institutions} : Create a new financialInstitution.
     *
     * @param financialInstitution the financialInstitution to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new financialInstitution, or with status {@code 400 (Bad Request)} if the financialInstitution has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/financial-institutions")
    public ResponseEntity<FinancialInstitution> createFinancialInstitution(@Valid @RequestBody FinancialInstitution financialInstitution) throws URISyntaxException {
        log.debug("REST request to save FinancialInstitution : {}", financialInstitution);
        if (financialInstitution.getId() != null) {
            throw new BadRequestAlertException("A new financialInstitution cannot already have an ID", ENTITY_NAME, "idexists");
        }
        FinancialInstitution result = financialInstitutionService.save(financialInstitution);
        return ResponseEntity.created(new URI("/api/financial-institutions/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /financial-institutions} : Updates an existing financialInstitution.
     *
     * @param financialInstitution the financialInstitution to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated financialInstitution,
     * or with status {@code 400 (Bad Request)} if the financialInstitution is not valid,
     * or with status {@code 500 (Internal Server Error)} if the financialInstitution couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/financial-institutions")
    public ResponseEntity<FinancialInstitution> updateFinancialInstitution(@Valid @RequestBody FinancialInstitution financialInstitution) throws URISyntaxException {
        log.debug("REST request to update FinancialInstitution : {}", financialInstitution);
        if (financialInstitution.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        FinancialInstitution result = financialInstitutionService.save(financialInstitution);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, financialInstitution.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /financial-institutions} : get all the financialInstitutions.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of financialInstitutions in body.
     */
    @GetMapping("/financial-institutions")
    public ResponseEntity<List<FinancialInstitution>> getAllFinancialInstitutions(FinancialInstitutionCriteria criteria, Pageable pageable) {
        log.debug("REST request to get FinancialInstitutions by criteria: {}", criteria);
        Page<FinancialInstitution> page = financialInstitutionQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /financial-institutions/count} : count all the financialInstitutions.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/financial-institutions/count")
    public ResponseEntity<Long> countFinancialInstitutions(FinancialInstitutionCriteria criteria) {
        log.debug("REST request to count FinancialInstitutions by criteria: {}", criteria);
        return ResponseEntity.ok().body(financialInstitutionQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /financial-institutions/:id} : get the "id" financialInstitution.
     *
     * @param id the id of the financialInstitution to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the financialInstitution, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/financial-institutions/{id}")
    public ResponseEntity<FinancialInstitution> getFinancialInstitution(@PathVariable Long id) {
        log.debug("REST request to get FinancialInstitution : {}", id);
        Optional<FinancialInstitution> financialInstitution = financialInstitutionService.findOne(id);
        return ResponseUtil.wrapOrNotFound(financialInstitution);
    }

    /**
     * {@code DELETE  /financial-institutions/:id} : delete the "id" financialInstitution.
     *
     * @param id the id of the financialInstitution to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/financial-institutions/{id}")
    public ResponseEntity<Void> deleteFinancialInstitution(@PathVariable Long id) {
        log.debug("REST request to delete FinancialInstitution : {}", id);
        financialInstitutionService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
