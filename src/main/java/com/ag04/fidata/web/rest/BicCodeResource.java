package com.ag04.fidata.web.rest;

import com.ag04.fidata.domain.BicCode;
import com.ag04.fidata.service.BicCodeService;
import com.ag04.fidata.web.rest.errors.BadRequestAlertException;
import com.ag04.fidata.service.dto.BicCodeCriteria;
import com.ag04.fidata.service.BicCodeQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.ag04.fidata.domain.BicCode}.
 */
@RestController
@RequestMapping("/api")
public class BicCodeResource {

    private final Logger log = LoggerFactory.getLogger(BicCodeResource.class);

    private static final String ENTITY_NAME = "fidataServiceBicCode";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final BicCodeService bicCodeService;

    private final BicCodeQueryService bicCodeQueryService;

    public BicCodeResource(BicCodeService bicCodeService, BicCodeQueryService bicCodeQueryService) {
        this.bicCodeService = bicCodeService;
        this.bicCodeQueryService = bicCodeQueryService;
    }

    /**
     * {@code POST  /bic-codes} : Create a new bicCode.
     *
     * @param bicCode the bicCode to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new bicCode, or with status {@code 400 (Bad Request)} if the bicCode has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/bic-codes")
    public ResponseEntity<BicCode> createBicCode(@RequestBody BicCode bicCode) throws URISyntaxException {
        log.debug("REST request to save BicCode : {}", bicCode);
        if (bicCode.getId() != null) {
            throw new BadRequestAlertException("A new bicCode cannot already have an ID", ENTITY_NAME, "idexists");
        }
        BicCode result = bicCodeService.save(bicCode);
        return ResponseEntity.created(new URI("/api/bic-codes/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /bic-codes} : Updates an existing bicCode.
     *
     * @param bicCode the bicCode to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated bicCode,
     * or with status {@code 400 (Bad Request)} if the bicCode is not valid,
     * or with status {@code 500 (Internal Server Error)} if the bicCode couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/bic-codes")
    public ResponseEntity<BicCode> updateBicCode(@RequestBody BicCode bicCode) throws URISyntaxException {
        log.debug("REST request to update BicCode : {}", bicCode);
        if (bicCode.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        BicCode result = bicCodeService.save(bicCode);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, bicCode.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /bic-codes} : get all the bicCodes.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of bicCodes in body.
     */
    @GetMapping("/bic-codes")
    public ResponseEntity<List<BicCode>> getAllBicCodes(BicCodeCriteria criteria, Pageable pageable) {
        log.debug("REST request to get BicCodes by criteria: {}", criteria);
        Page<BicCode> page = bicCodeQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /bic-codes/count} : count all the bicCodes.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/bic-codes/count")
    public ResponseEntity<Long> countBicCodes(BicCodeCriteria criteria) {
        log.debug("REST request to count BicCodes by criteria: {}", criteria);
        return ResponseEntity.ok().body(bicCodeQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /bic-codes/:id} : get the "id" bicCode.
     *
     * @param id the id of the bicCode to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the bicCode, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/bic-codes/{id}")
    public ResponseEntity<BicCode> getBicCode(@PathVariable Long id) {
        log.debug("REST request to get BicCode : {}", id);
        Optional<BicCode> bicCode = bicCodeService.findOne(id);
        return ResponseUtil.wrapOrNotFound(bicCode);
    }

    /**
     * {@code DELETE  /bic-codes/:id} : delete the "id" bicCode.
     *
     * @param id the id of the bicCode to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/bic-codes/{id}")
    public ResponseEntity<Void> deleteBicCode(@PathVariable Long id) {
        log.debug("REST request to delete BicCode : {}", id);
        bicCodeService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
