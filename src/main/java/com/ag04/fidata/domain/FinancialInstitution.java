package com.ag04.fidata.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * A FinancialInstitution.
 */
@Entity
@Table(name = "fin_inst")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class FinancialInstitution implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "description")
    private String description;

    @Column(name = "address")
    private String address;

    @Column(name = "address_2")
    private String address2;

    @Column(name = "city")
    private String city;

    @Column(name = "state")
    private String state;

    @Column(name = "postal_code")
    private String postalCode;

    @OneToMany(mappedBy = "financialInstitution")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    private Set<BicCode> bicCodes = new HashSet<>();

    @ManyToOne
    @JsonIgnoreProperties(value = "financialInstitutions", allowSetters = true)
    private Country country;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public FinancialInstitution name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public FinancialInstitution description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAddress() {
        return address;
    }

    public FinancialInstitution address(String address) {
        this.address = address;
        return this;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAddress2() {
        return address2;
    }

    public FinancialInstitution address2(String address2) {
        this.address2 = address2;
        return this;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getCity() {
        return city;
    }

    public FinancialInstitution city(String city) {
        this.city = city;
        return this;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public FinancialInstitution state(String state) {
        this.state = state;
        return this;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public FinancialInstitution postalCode(String postalCode) {
        this.postalCode = postalCode;
        return this;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public Set<BicCode> getBicCodes() {
        return bicCodes;
    }

    public FinancialInstitution bicCodes(Set<BicCode> bicCodes) {
        this.bicCodes = bicCodes;
        return this;
    }

    public FinancialInstitution addBicCodes(BicCode bicCode) {
        this.bicCodes.add(bicCode);
        bicCode.setFinancialInstitution(this);
        return this;
    }

    public FinancialInstitution removeBicCodes(BicCode bicCode) {
        this.bicCodes.remove(bicCode);
        bicCode.setFinancialInstitution(null);
        return this;
    }

    public void setBicCodes(Set<BicCode> bicCodes) {
        this.bicCodes = bicCodes;
    }

    public Country getCountry() {
        return country;
    }

    public FinancialInstitution country(Country country) {
        this.country = country;
        return this;
    }

    public void setCountry(Country country) {
        this.country = country;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof FinancialInstitution)) {
            return false;
        }
        return id != null && id.equals(((FinancialInstitution) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "FinancialInstitution{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", description='" + getDescription() + "'" +
            ", address='" + getAddress() + "'" +
            ", address2='" + getAddress2() + "'" +
            ", city='" + getCity() + "'" +
            ", state='" + getState() + "'" +
            ", postalCode='" + getPostalCode() + "'" +
            "}";
    }
}
