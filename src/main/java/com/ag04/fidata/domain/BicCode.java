package com.ag04.fidata.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;

/**
 * A BicCode.
 */
@Entity
@Table(name = "breg_bic_code")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class BicCode implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "swift_code")
    private String swiftCode;

    @Column(name = "city")
    private String city;

    @Column(name = "branch")
    private String branch;

    @Column(name = "address")
    private String address;

    @Column(name = "postal_code")
    private String postalCode;

    @Column(name = "bic_lat")
    private Double bicLat;

    @Column(name = "bic_lng")
    private Double bicLng;

    @ManyToOne
    @JsonIgnoreProperties(value = "bicCodes", allowSetters = true)
    private Country country;

    @ManyToOne
    @JsonIgnoreProperties(value = "bicCodes", allowSetters = true)
    private FinancialInstitution financialInstitution;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSwiftCode() {
        return swiftCode;
    }

    public BicCode swiftCode(String swiftCode) {
        this.swiftCode = swiftCode;
        return this;
    }

    public void setSwiftCode(String swiftCode) {
        this.swiftCode = swiftCode;
    }

    public String getCity() {
        return city;
    }

    public BicCode city(String city) {
        this.city = city;
        return this;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getBranch() {
        return branch;
    }

    public BicCode branch(String branch) {
        this.branch = branch;
        return this;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }

    public String getAddress() {
        return address;
    }

    public BicCode address(String address) {
        this.address = address;
        return this;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public BicCode postalCode(String postalCode) {
        this.postalCode = postalCode;
        return this;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public Double getBicLat() {
        return bicLat;
    }

    public BicCode bicLat(Double bicLat) {
        this.bicLat = bicLat;
        return this;
    }

    public void setBicLat(Double bicLat) {
        this.bicLat = bicLat;
    }

    public Double getBicLng() {
        return bicLng;
    }

    public BicCode bicLng(Double bicLng) {
        this.bicLng = bicLng;
        return this;
    }

    public void setBicLng(Double bicLng) {
        this.bicLng = bicLng;
    }

    public Country getCountry() {
        return country;
    }

    public BicCode country(Country country) {
        this.country = country;
        return this;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public FinancialInstitution getFinancialInstitution() {
        return financialInstitution;
    }

    public BicCode financialInstitution(FinancialInstitution financialInstitution) {
        this.financialInstitution = financialInstitution;
        return this;
    }

    public void setFinancialInstitution(FinancialInstitution financialInstitution) {
        this.financialInstitution = financialInstitution;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof BicCode)) {
            return false;
        }
        return id != null && id.equals(((BicCode) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "BicCode{" +
            "id=" + getId() +
            ", swiftCode='" + getSwiftCode() + "'" +
            ", city='" + getCity() + "'" +
            ", branch='" + getBranch() + "'" +
            ", address='" + getAddress() + "'" +
            ", postalCode='" + getPostalCode() + "'" +
            ", bicLat=" + getBicLat() +
            ", bicLng=" + getBicLng() +
            "}";
    }
}
