package com.ag04.fidata.service;

import com.ag04.fidata.domain.BicCode;
import com.ag04.fidata.repository.BicCodeRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link BicCode}.
 */
@Service
@Transactional
public class BicCodeService {

    private final Logger log = LoggerFactory.getLogger(BicCodeService.class);

    private final BicCodeRepository bicCodeRepository;

    public BicCodeService(BicCodeRepository bicCodeRepository) {
        this.bicCodeRepository = bicCodeRepository;
    }

    /**
     * Save a bicCode.
     *
     * @param bicCode the entity to save.
     * @return the persisted entity.
     */
    public BicCode save(BicCode bicCode) {
        log.debug("Request to save BicCode : {}", bicCode);
        return bicCodeRepository.save(bicCode);
    }

    /**
     * Get all the bicCodes.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<BicCode> findAll(Pageable pageable) {
        log.debug("Request to get all BicCodes");
        return bicCodeRepository.findAll(pageable);
    }


    /**
     * Get one bicCode by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<BicCode> findOne(Long id) {
        log.debug("Request to get BicCode : {}", id);
        return bicCodeRepository.findById(id);
    }

    /**
     * Delete the bicCode by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete BicCode : {}", id);
        bicCodeRepository.deleteById(id);
    }
}
