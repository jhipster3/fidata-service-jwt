package com.ag04.fidata.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.ag04.fidata.domain.FinancialInstitution;
import com.ag04.fidata.domain.*; // for static metamodels
import com.ag04.fidata.repository.FinancialInstitutionRepository;
import com.ag04.fidata.service.dto.FinancialInstitutionCriteria;

/**
 * Service for executing complex queries for {@link FinancialInstitution} entities in the database.
 * The main input is a {@link FinancialInstitutionCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link FinancialInstitution} or a {@link Page} of {@link FinancialInstitution} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class FinancialInstitutionQueryService extends QueryService<FinancialInstitution> {

    private final Logger log = LoggerFactory.getLogger(FinancialInstitutionQueryService.class);

    private final FinancialInstitutionRepository financialInstitutionRepository;

    public FinancialInstitutionQueryService(FinancialInstitutionRepository financialInstitutionRepository) {
        this.financialInstitutionRepository = financialInstitutionRepository;
    }

    /**
     * Return a {@link List} of {@link FinancialInstitution} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<FinancialInstitution> findByCriteria(FinancialInstitutionCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<FinancialInstitution> specification = createSpecification(criteria);
        return financialInstitutionRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link FinancialInstitution} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<FinancialInstitution> findByCriteria(FinancialInstitutionCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<FinancialInstitution> specification = createSpecification(criteria);
        return financialInstitutionRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(FinancialInstitutionCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<FinancialInstitution> specification = createSpecification(criteria);
        return financialInstitutionRepository.count(specification);
    }

    /**
     * Function to convert {@link FinancialInstitutionCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<FinancialInstitution> createSpecification(FinancialInstitutionCriteria criteria) {
        Specification<FinancialInstitution> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), FinancialInstitution_.id));
            }
            if (criteria.getName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getName(), FinancialInstitution_.name));
            }
            if (criteria.getDescription() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDescription(), FinancialInstitution_.description));
            }
            if (criteria.getAddress() != null) {
                specification = specification.and(buildStringSpecification(criteria.getAddress(), FinancialInstitution_.address));
            }
            if (criteria.getAddress2() != null) {
                specification = specification.and(buildStringSpecification(criteria.getAddress2(), FinancialInstitution_.address2));
            }
            if (criteria.getCity() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCity(), FinancialInstitution_.city));
            }
            if (criteria.getState() != null) {
                specification = specification.and(buildStringSpecification(criteria.getState(), FinancialInstitution_.state));
            }
            if (criteria.getPostalCode() != null) {
                specification = specification.and(buildStringSpecification(criteria.getPostalCode(), FinancialInstitution_.postalCode));
            }
            if (criteria.getBicCodesId() != null) {
                specification = specification.and(buildSpecification(criteria.getBicCodesId(),
                    root -> root.join(FinancialInstitution_.bicCodes, JoinType.LEFT).get(BicCode_.id)));
            }
            if (criteria.getCountryId() != null) {
                specification = specification.and(buildSpecification(criteria.getCountryId(),
                    root -> root.join(FinancialInstitution_.country, JoinType.LEFT).get(Country_.id)));
            }
        }
        return specification;
    }
}
