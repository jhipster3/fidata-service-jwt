package com.ag04.fidata.service;

import com.ag04.fidata.domain.FinancialInstitution;
import com.ag04.fidata.repository.FinancialInstitutionRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link FinancialInstitution}.
 */
@Service
@Transactional
public class FinancialInstitutionService {

    private final Logger log = LoggerFactory.getLogger(FinancialInstitutionService.class);

    private final FinancialInstitutionRepository financialInstitutionRepository;

    public FinancialInstitutionService(FinancialInstitutionRepository financialInstitutionRepository) {
        this.financialInstitutionRepository = financialInstitutionRepository;
    }

    /**
     * Save a financialInstitution.
     *
     * @param financialInstitution the entity to save.
     * @return the persisted entity.
     */
    public FinancialInstitution save(FinancialInstitution financialInstitution) {
        log.debug("Request to save FinancialInstitution : {}", financialInstitution);
        return financialInstitutionRepository.save(financialInstitution);
    }

    /**
     * Get all the financialInstitutions.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<FinancialInstitution> findAll(Pageable pageable) {
        log.debug("Request to get all FinancialInstitutions");
        return financialInstitutionRepository.findAll(pageable);
    }


    /**
     * Get one financialInstitution by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<FinancialInstitution> findOne(Long id) {
        log.debug("Request to get FinancialInstitution : {}", id);
        return financialInstitutionRepository.findById(id);
    }

    /**
     * Delete the financialInstitution by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete FinancialInstitution : {}", id);
        financialInstitutionRepository.deleteById(id);
    }
}
