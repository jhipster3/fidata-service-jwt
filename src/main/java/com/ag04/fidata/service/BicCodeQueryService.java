package com.ag04.fidata.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.ag04.fidata.domain.BicCode;
import com.ag04.fidata.domain.*; // for static metamodels
import com.ag04.fidata.repository.BicCodeRepository;
import com.ag04.fidata.service.dto.BicCodeCriteria;

/**
 * Service for executing complex queries for {@link BicCode} entities in the database.
 * The main input is a {@link BicCodeCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link BicCode} or a {@link Page} of {@link BicCode} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class BicCodeQueryService extends QueryService<BicCode> {

    private final Logger log = LoggerFactory.getLogger(BicCodeQueryService.class);

    private final BicCodeRepository bicCodeRepository;

    public BicCodeQueryService(BicCodeRepository bicCodeRepository) {
        this.bicCodeRepository = bicCodeRepository;
    }

    /**
     * Return a {@link List} of {@link BicCode} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<BicCode> findByCriteria(BicCodeCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<BicCode> specification = createSpecification(criteria);
        return bicCodeRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link BicCode} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<BicCode> findByCriteria(BicCodeCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<BicCode> specification = createSpecification(criteria);
        return bicCodeRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(BicCodeCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<BicCode> specification = createSpecification(criteria);
        return bicCodeRepository.count(specification);
    }

    /**
     * Function to convert {@link BicCodeCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<BicCode> createSpecification(BicCodeCriteria criteria) {
        Specification<BicCode> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), BicCode_.id));
            }
            if (criteria.getSwiftCode() != null) {
                specification = specification.and(buildStringSpecification(criteria.getSwiftCode(), BicCode_.swiftCode));
            }
            if (criteria.getCity() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCity(), BicCode_.city));
            }
            if (criteria.getBranch() != null) {
                specification = specification.and(buildStringSpecification(criteria.getBranch(), BicCode_.branch));
            }
            if (criteria.getAddress() != null) {
                specification = specification.and(buildStringSpecification(criteria.getAddress(), BicCode_.address));
            }
            if (criteria.getPostalCode() != null) {
                specification = specification.and(buildStringSpecification(criteria.getPostalCode(), BicCode_.postalCode));
            }
            if (criteria.getBicLat() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getBicLat(), BicCode_.bicLat));
            }
            if (criteria.getBicLng() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getBicLng(), BicCode_.bicLng));
            }
            if (criteria.getCountryId() != null) {
                specification = specification.and(buildSpecification(criteria.getCountryId(),
                    root -> root.join(BicCode_.country, JoinType.LEFT).get(Country_.id)));
            }
            if (criteria.getFinancialInstitutionId() != null) {
                specification = specification.and(buildSpecification(criteria.getFinancialInstitutionId(),
                    root -> root.join(BicCode_.financialInstitution, JoinType.LEFT).get(FinancialInstitution_.id)));
            }
        }
        return specification;
    }
}
