package com.ag04.fidata.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link com.ag04.fidata.domain.BicCode} entity. This class is used
 * in {@link com.ag04.fidata.web.rest.BicCodeResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /bic-codes?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class BicCodeCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter swiftCode;

    private StringFilter city;

    private StringFilter branch;

    private StringFilter address;

    private StringFilter postalCode;

    private DoubleFilter bicLat;

    private DoubleFilter bicLng;

    private LongFilter countryId;

    private LongFilter financialInstitutionId;

    public BicCodeCriteria() {
    }

    public BicCodeCriteria(BicCodeCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.swiftCode = other.swiftCode == null ? null : other.swiftCode.copy();
        this.city = other.city == null ? null : other.city.copy();
        this.branch = other.branch == null ? null : other.branch.copy();
        this.address = other.address == null ? null : other.address.copy();
        this.postalCode = other.postalCode == null ? null : other.postalCode.copy();
        this.bicLat = other.bicLat == null ? null : other.bicLat.copy();
        this.bicLng = other.bicLng == null ? null : other.bicLng.copy();
        this.countryId = other.countryId == null ? null : other.countryId.copy();
        this.financialInstitutionId = other.financialInstitutionId == null ? null : other.financialInstitutionId.copy();
    }

    @Override
    public BicCodeCriteria copy() {
        return new BicCodeCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getSwiftCode() {
        return swiftCode;
    }

    public void setSwiftCode(StringFilter swiftCode) {
        this.swiftCode = swiftCode;
    }

    public StringFilter getCity() {
        return city;
    }

    public void setCity(StringFilter city) {
        this.city = city;
    }

    public StringFilter getBranch() {
        return branch;
    }

    public void setBranch(StringFilter branch) {
        this.branch = branch;
    }

    public StringFilter getAddress() {
        return address;
    }

    public void setAddress(StringFilter address) {
        this.address = address;
    }

    public StringFilter getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(StringFilter postalCode) {
        this.postalCode = postalCode;
    }

    public DoubleFilter getBicLat() {
        return bicLat;
    }

    public void setBicLat(DoubleFilter bicLat) {
        this.bicLat = bicLat;
    }

    public DoubleFilter getBicLng() {
        return bicLng;
    }

    public void setBicLng(DoubleFilter bicLng) {
        this.bicLng = bicLng;
    }

    public LongFilter getCountryId() {
        return countryId;
    }

    public void setCountryId(LongFilter countryId) {
        this.countryId = countryId;
    }

    public LongFilter getFinancialInstitutionId() {
        return financialInstitutionId;
    }

    public void setFinancialInstitutionId(LongFilter financialInstitutionId) {
        this.financialInstitutionId = financialInstitutionId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final BicCodeCriteria that = (BicCodeCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(swiftCode, that.swiftCode) &&
            Objects.equals(city, that.city) &&
            Objects.equals(branch, that.branch) &&
            Objects.equals(address, that.address) &&
            Objects.equals(postalCode, that.postalCode) &&
            Objects.equals(bicLat, that.bicLat) &&
            Objects.equals(bicLng, that.bicLng) &&
            Objects.equals(countryId, that.countryId) &&
            Objects.equals(financialInstitutionId, that.financialInstitutionId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        swiftCode,
        city,
        branch,
        address,
        postalCode,
        bicLat,
        bicLng,
        countryId,
        financialInstitutionId
        );
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "BicCodeCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (swiftCode != null ? "swiftCode=" + swiftCode + ", " : "") +
                (city != null ? "city=" + city + ", " : "") +
                (branch != null ? "branch=" + branch + ", " : "") +
                (address != null ? "address=" + address + ", " : "") +
                (postalCode != null ? "postalCode=" + postalCode + ", " : "") +
                (bicLat != null ? "bicLat=" + bicLat + ", " : "") +
                (bicLng != null ? "bicLng=" + bicLng + ", " : "") +
                (countryId != null ? "countryId=" + countryId + ", " : "") +
                (financialInstitutionId != null ? "financialInstitutionId=" + financialInstitutionId + ", " : "") +
            "}";
    }

}
