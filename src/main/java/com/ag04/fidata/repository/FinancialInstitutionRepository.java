package com.ag04.fidata.repository;

import com.ag04.fidata.domain.FinancialInstitution;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the FinancialInstitution entity.
 */
@SuppressWarnings("unused")
@Repository
public interface FinancialInstitutionRepository extends JpaRepository<FinancialInstitution, Long>, JpaSpecificationExecutor<FinancialInstitution> {
}
