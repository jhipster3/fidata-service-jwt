package com.ag04.fidata.repository;

import com.ag04.fidata.domain.BicCode;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the BicCode entity.
 */
@SuppressWarnings("unused")
@Repository
public interface BicCodeRepository extends JpaRepository<BicCode, Long>, JpaSpecificationExecutor<BicCode> {
}
