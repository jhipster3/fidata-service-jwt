package com.ag04.fidata.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.ag04.fidata.web.rest.TestUtil;

public class BicCodeTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(BicCode.class);
        BicCode bicCode1 = new BicCode();
        bicCode1.setId(1L);
        BicCode bicCode2 = new BicCode();
        bicCode2.setId(bicCode1.getId());
        assertThat(bicCode1).isEqualTo(bicCode2);
        bicCode2.setId(2L);
        assertThat(bicCode1).isNotEqualTo(bicCode2);
        bicCode1.setId(null);
        assertThat(bicCode1).isNotEqualTo(bicCode2);
    }
}
