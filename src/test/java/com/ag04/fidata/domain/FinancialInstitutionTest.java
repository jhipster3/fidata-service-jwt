package com.ag04.fidata.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.ag04.fidata.web.rest.TestUtil;

public class FinancialInstitutionTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(FinancialInstitution.class);
        FinancialInstitution financialInstitution1 = new FinancialInstitution();
        financialInstitution1.setId(1L);
        FinancialInstitution financialInstitution2 = new FinancialInstitution();
        financialInstitution2.setId(financialInstitution1.getId());
        assertThat(financialInstitution1).isEqualTo(financialInstitution2);
        financialInstitution2.setId(2L);
        assertThat(financialInstitution1).isNotEqualTo(financialInstitution2);
        financialInstitution1.setId(null);
        assertThat(financialInstitution1).isNotEqualTo(financialInstitution2);
    }
}
