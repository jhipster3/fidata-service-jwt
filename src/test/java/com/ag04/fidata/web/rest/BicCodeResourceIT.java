package com.ag04.fidata.web.rest;

import com.ag04.fidata.FidataServiceApp;
import com.ag04.fidata.domain.BicCode;
import com.ag04.fidata.domain.Country;
import com.ag04.fidata.domain.FinancialInstitution;
import com.ag04.fidata.repository.BicCodeRepository;
import com.ag04.fidata.service.BicCodeService;
import com.ag04.fidata.service.dto.BicCodeCriteria;
import com.ag04.fidata.service.BicCodeQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link BicCodeResource} REST controller.
 */
@SpringBootTest(classes = FidataServiceApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class BicCodeResourceIT {

    private static final String DEFAULT_SWIFT_CODE = "AAAAAAAAAA";
    private static final String UPDATED_SWIFT_CODE = "BBBBBBBBBB";

    private static final String DEFAULT_CITY = "AAAAAAAAAA";
    private static final String UPDATED_CITY = "BBBBBBBBBB";

    private static final String DEFAULT_BRANCH = "AAAAAAAAAA";
    private static final String UPDATED_BRANCH = "BBBBBBBBBB";

    private static final String DEFAULT_ADDRESS = "AAAAAAAAAA";
    private static final String UPDATED_ADDRESS = "BBBBBBBBBB";

    private static final String DEFAULT_POSTAL_CODE = "AAAAAAAAAA";
    private static final String UPDATED_POSTAL_CODE = "BBBBBBBBBB";

    private static final Double DEFAULT_BIC_LAT = 1D;
    private static final Double UPDATED_BIC_LAT = 2D;
    private static final Double SMALLER_BIC_LAT = 1D - 1D;

    private static final Double DEFAULT_BIC_LNG = 1D;
    private static final Double UPDATED_BIC_LNG = 2D;
    private static final Double SMALLER_BIC_LNG = 1D - 1D;

    @Autowired
    private BicCodeRepository bicCodeRepository;

    @Autowired
    private BicCodeService bicCodeService;

    @Autowired
    private BicCodeQueryService bicCodeQueryService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restBicCodeMockMvc;

    private BicCode bicCode;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static BicCode createEntity(EntityManager em) {
        BicCode bicCode = new BicCode()
            .swiftCode(DEFAULT_SWIFT_CODE)
            .city(DEFAULT_CITY)
            .branch(DEFAULT_BRANCH)
            .address(DEFAULT_ADDRESS)
            .postalCode(DEFAULT_POSTAL_CODE)
            .bicLat(DEFAULT_BIC_LAT)
            .bicLng(DEFAULT_BIC_LNG);
        return bicCode;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static BicCode createUpdatedEntity(EntityManager em) {
        BicCode bicCode = new BicCode()
            .swiftCode(UPDATED_SWIFT_CODE)
            .city(UPDATED_CITY)
            .branch(UPDATED_BRANCH)
            .address(UPDATED_ADDRESS)
            .postalCode(UPDATED_POSTAL_CODE)
            .bicLat(UPDATED_BIC_LAT)
            .bicLng(UPDATED_BIC_LNG);
        return bicCode;
    }

    @BeforeEach
    public void initTest() {
        bicCode = createEntity(em);
    }

    @Test
    @Transactional
    public void createBicCode() throws Exception {
        int databaseSizeBeforeCreate = bicCodeRepository.findAll().size();
        // Create the BicCode
        restBicCodeMockMvc.perform(post("/api/bic-codes")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(bicCode)))
            .andExpect(status().isCreated());

        // Validate the BicCode in the database
        List<BicCode> bicCodeList = bicCodeRepository.findAll();
        assertThat(bicCodeList).hasSize(databaseSizeBeforeCreate + 1);
        BicCode testBicCode = bicCodeList.get(bicCodeList.size() - 1);
        assertThat(testBicCode.getSwiftCode()).isEqualTo(DEFAULT_SWIFT_CODE);
        assertThat(testBicCode.getCity()).isEqualTo(DEFAULT_CITY);
        assertThat(testBicCode.getBranch()).isEqualTo(DEFAULT_BRANCH);
        assertThat(testBicCode.getAddress()).isEqualTo(DEFAULT_ADDRESS);
        assertThat(testBicCode.getPostalCode()).isEqualTo(DEFAULT_POSTAL_CODE);
        assertThat(testBicCode.getBicLat()).isEqualTo(DEFAULT_BIC_LAT);
        assertThat(testBicCode.getBicLng()).isEqualTo(DEFAULT_BIC_LNG);
    }

    @Test
    @Transactional
    public void createBicCodeWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = bicCodeRepository.findAll().size();

        // Create the BicCode with an existing ID
        bicCode.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restBicCodeMockMvc.perform(post("/api/bic-codes")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(bicCode)))
            .andExpect(status().isBadRequest());

        // Validate the BicCode in the database
        List<BicCode> bicCodeList = bicCodeRepository.findAll();
        assertThat(bicCodeList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllBicCodes() throws Exception {
        // Initialize the database
        bicCodeRepository.saveAndFlush(bicCode);

        // Get all the bicCodeList
        restBicCodeMockMvc.perform(get("/api/bic-codes?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(bicCode.getId().intValue())))
            .andExpect(jsonPath("$.[*].swiftCode").value(hasItem(DEFAULT_SWIFT_CODE)))
            .andExpect(jsonPath("$.[*].city").value(hasItem(DEFAULT_CITY)))
            .andExpect(jsonPath("$.[*].branch").value(hasItem(DEFAULT_BRANCH)))
            .andExpect(jsonPath("$.[*].address").value(hasItem(DEFAULT_ADDRESS)))
            .andExpect(jsonPath("$.[*].postalCode").value(hasItem(DEFAULT_POSTAL_CODE)))
            .andExpect(jsonPath("$.[*].bicLat").value(hasItem(DEFAULT_BIC_LAT.doubleValue())))
            .andExpect(jsonPath("$.[*].bicLng").value(hasItem(DEFAULT_BIC_LNG.doubleValue())));
    }

    @Test
    @Transactional
    public void getBicCode() throws Exception {
        // Initialize the database
        bicCodeRepository.saveAndFlush(bicCode);

        // Get the bicCode
        restBicCodeMockMvc.perform(get("/api/bic-codes/{id}", bicCode.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(bicCode.getId().intValue()))
            .andExpect(jsonPath("$.swiftCode").value(DEFAULT_SWIFT_CODE))
            .andExpect(jsonPath("$.city").value(DEFAULT_CITY))
            .andExpect(jsonPath("$.branch").value(DEFAULT_BRANCH))
            .andExpect(jsonPath("$.address").value(DEFAULT_ADDRESS))
            .andExpect(jsonPath("$.postalCode").value(DEFAULT_POSTAL_CODE))
            .andExpect(jsonPath("$.bicLat").value(DEFAULT_BIC_LAT.doubleValue()))
            .andExpect(jsonPath("$.bicLng").value(DEFAULT_BIC_LNG.doubleValue()));
    }


    @Test
    @Transactional
    public void getBicCodesByIdFiltering() throws Exception {
        // Initialize the database
        bicCodeRepository.saveAndFlush(bicCode);

        Long id = bicCode.getId();

        defaultBicCodeShouldBeFound("id.equals=" + id);
        defaultBicCodeShouldNotBeFound("id.notEquals=" + id);

        defaultBicCodeShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultBicCodeShouldNotBeFound("id.greaterThan=" + id);

        defaultBicCodeShouldBeFound("id.lessThanOrEqual=" + id);
        defaultBicCodeShouldNotBeFound("id.lessThan=" + id);
    }


    @Test
    @Transactional
    public void getAllBicCodesBySwiftCodeIsEqualToSomething() throws Exception {
        // Initialize the database
        bicCodeRepository.saveAndFlush(bicCode);

        // Get all the bicCodeList where swiftCode equals to DEFAULT_SWIFT_CODE
        defaultBicCodeShouldBeFound("swiftCode.equals=" + DEFAULT_SWIFT_CODE);

        // Get all the bicCodeList where swiftCode equals to UPDATED_SWIFT_CODE
        defaultBicCodeShouldNotBeFound("swiftCode.equals=" + UPDATED_SWIFT_CODE);
    }

    @Test
    @Transactional
    public void getAllBicCodesBySwiftCodeIsNotEqualToSomething() throws Exception {
        // Initialize the database
        bicCodeRepository.saveAndFlush(bicCode);

        // Get all the bicCodeList where swiftCode not equals to DEFAULT_SWIFT_CODE
        defaultBicCodeShouldNotBeFound("swiftCode.notEquals=" + DEFAULT_SWIFT_CODE);

        // Get all the bicCodeList where swiftCode not equals to UPDATED_SWIFT_CODE
        defaultBicCodeShouldBeFound("swiftCode.notEquals=" + UPDATED_SWIFT_CODE);
    }

    @Test
    @Transactional
    public void getAllBicCodesBySwiftCodeIsInShouldWork() throws Exception {
        // Initialize the database
        bicCodeRepository.saveAndFlush(bicCode);

        // Get all the bicCodeList where swiftCode in DEFAULT_SWIFT_CODE or UPDATED_SWIFT_CODE
        defaultBicCodeShouldBeFound("swiftCode.in=" + DEFAULT_SWIFT_CODE + "," + UPDATED_SWIFT_CODE);

        // Get all the bicCodeList where swiftCode equals to UPDATED_SWIFT_CODE
        defaultBicCodeShouldNotBeFound("swiftCode.in=" + UPDATED_SWIFT_CODE);
    }

    @Test
    @Transactional
    public void getAllBicCodesBySwiftCodeIsNullOrNotNull() throws Exception {
        // Initialize the database
        bicCodeRepository.saveAndFlush(bicCode);

        // Get all the bicCodeList where swiftCode is not null
        defaultBicCodeShouldBeFound("swiftCode.specified=true");

        // Get all the bicCodeList where swiftCode is null
        defaultBicCodeShouldNotBeFound("swiftCode.specified=false");
    }
                @Test
    @Transactional
    public void getAllBicCodesBySwiftCodeContainsSomething() throws Exception {
        // Initialize the database
        bicCodeRepository.saveAndFlush(bicCode);

        // Get all the bicCodeList where swiftCode contains DEFAULT_SWIFT_CODE
        defaultBicCodeShouldBeFound("swiftCode.contains=" + DEFAULT_SWIFT_CODE);

        // Get all the bicCodeList where swiftCode contains UPDATED_SWIFT_CODE
        defaultBicCodeShouldNotBeFound("swiftCode.contains=" + UPDATED_SWIFT_CODE);
    }

    @Test
    @Transactional
    public void getAllBicCodesBySwiftCodeNotContainsSomething() throws Exception {
        // Initialize the database
        bicCodeRepository.saveAndFlush(bicCode);

        // Get all the bicCodeList where swiftCode does not contain DEFAULT_SWIFT_CODE
        defaultBicCodeShouldNotBeFound("swiftCode.doesNotContain=" + DEFAULT_SWIFT_CODE);

        // Get all the bicCodeList where swiftCode does not contain UPDATED_SWIFT_CODE
        defaultBicCodeShouldBeFound("swiftCode.doesNotContain=" + UPDATED_SWIFT_CODE);
    }


    @Test
    @Transactional
    public void getAllBicCodesByCityIsEqualToSomething() throws Exception {
        // Initialize the database
        bicCodeRepository.saveAndFlush(bicCode);

        // Get all the bicCodeList where city equals to DEFAULT_CITY
        defaultBicCodeShouldBeFound("city.equals=" + DEFAULT_CITY);

        // Get all the bicCodeList where city equals to UPDATED_CITY
        defaultBicCodeShouldNotBeFound("city.equals=" + UPDATED_CITY);
    }

    @Test
    @Transactional
    public void getAllBicCodesByCityIsNotEqualToSomething() throws Exception {
        // Initialize the database
        bicCodeRepository.saveAndFlush(bicCode);

        // Get all the bicCodeList where city not equals to DEFAULT_CITY
        defaultBicCodeShouldNotBeFound("city.notEquals=" + DEFAULT_CITY);

        // Get all the bicCodeList where city not equals to UPDATED_CITY
        defaultBicCodeShouldBeFound("city.notEquals=" + UPDATED_CITY);
    }

    @Test
    @Transactional
    public void getAllBicCodesByCityIsInShouldWork() throws Exception {
        // Initialize the database
        bicCodeRepository.saveAndFlush(bicCode);

        // Get all the bicCodeList where city in DEFAULT_CITY or UPDATED_CITY
        defaultBicCodeShouldBeFound("city.in=" + DEFAULT_CITY + "," + UPDATED_CITY);

        // Get all the bicCodeList where city equals to UPDATED_CITY
        defaultBicCodeShouldNotBeFound("city.in=" + UPDATED_CITY);
    }

    @Test
    @Transactional
    public void getAllBicCodesByCityIsNullOrNotNull() throws Exception {
        // Initialize the database
        bicCodeRepository.saveAndFlush(bicCode);

        // Get all the bicCodeList where city is not null
        defaultBicCodeShouldBeFound("city.specified=true");

        // Get all the bicCodeList where city is null
        defaultBicCodeShouldNotBeFound("city.specified=false");
    }
                @Test
    @Transactional
    public void getAllBicCodesByCityContainsSomething() throws Exception {
        // Initialize the database
        bicCodeRepository.saveAndFlush(bicCode);

        // Get all the bicCodeList where city contains DEFAULT_CITY
        defaultBicCodeShouldBeFound("city.contains=" + DEFAULT_CITY);

        // Get all the bicCodeList where city contains UPDATED_CITY
        defaultBicCodeShouldNotBeFound("city.contains=" + UPDATED_CITY);
    }

    @Test
    @Transactional
    public void getAllBicCodesByCityNotContainsSomething() throws Exception {
        // Initialize the database
        bicCodeRepository.saveAndFlush(bicCode);

        // Get all the bicCodeList where city does not contain DEFAULT_CITY
        defaultBicCodeShouldNotBeFound("city.doesNotContain=" + DEFAULT_CITY);

        // Get all the bicCodeList where city does not contain UPDATED_CITY
        defaultBicCodeShouldBeFound("city.doesNotContain=" + UPDATED_CITY);
    }


    @Test
    @Transactional
    public void getAllBicCodesByBranchIsEqualToSomething() throws Exception {
        // Initialize the database
        bicCodeRepository.saveAndFlush(bicCode);

        // Get all the bicCodeList where branch equals to DEFAULT_BRANCH
        defaultBicCodeShouldBeFound("branch.equals=" + DEFAULT_BRANCH);

        // Get all the bicCodeList where branch equals to UPDATED_BRANCH
        defaultBicCodeShouldNotBeFound("branch.equals=" + UPDATED_BRANCH);
    }

    @Test
    @Transactional
    public void getAllBicCodesByBranchIsNotEqualToSomething() throws Exception {
        // Initialize the database
        bicCodeRepository.saveAndFlush(bicCode);

        // Get all the bicCodeList where branch not equals to DEFAULT_BRANCH
        defaultBicCodeShouldNotBeFound("branch.notEquals=" + DEFAULT_BRANCH);

        // Get all the bicCodeList where branch not equals to UPDATED_BRANCH
        defaultBicCodeShouldBeFound("branch.notEquals=" + UPDATED_BRANCH);
    }

    @Test
    @Transactional
    public void getAllBicCodesByBranchIsInShouldWork() throws Exception {
        // Initialize the database
        bicCodeRepository.saveAndFlush(bicCode);

        // Get all the bicCodeList where branch in DEFAULT_BRANCH or UPDATED_BRANCH
        defaultBicCodeShouldBeFound("branch.in=" + DEFAULT_BRANCH + "," + UPDATED_BRANCH);

        // Get all the bicCodeList where branch equals to UPDATED_BRANCH
        defaultBicCodeShouldNotBeFound("branch.in=" + UPDATED_BRANCH);
    }

    @Test
    @Transactional
    public void getAllBicCodesByBranchIsNullOrNotNull() throws Exception {
        // Initialize the database
        bicCodeRepository.saveAndFlush(bicCode);

        // Get all the bicCodeList where branch is not null
        defaultBicCodeShouldBeFound("branch.specified=true");

        // Get all the bicCodeList where branch is null
        defaultBicCodeShouldNotBeFound("branch.specified=false");
    }
                @Test
    @Transactional
    public void getAllBicCodesByBranchContainsSomething() throws Exception {
        // Initialize the database
        bicCodeRepository.saveAndFlush(bicCode);

        // Get all the bicCodeList where branch contains DEFAULT_BRANCH
        defaultBicCodeShouldBeFound("branch.contains=" + DEFAULT_BRANCH);

        // Get all the bicCodeList where branch contains UPDATED_BRANCH
        defaultBicCodeShouldNotBeFound("branch.contains=" + UPDATED_BRANCH);
    }

    @Test
    @Transactional
    public void getAllBicCodesByBranchNotContainsSomething() throws Exception {
        // Initialize the database
        bicCodeRepository.saveAndFlush(bicCode);

        // Get all the bicCodeList where branch does not contain DEFAULT_BRANCH
        defaultBicCodeShouldNotBeFound("branch.doesNotContain=" + DEFAULT_BRANCH);

        // Get all the bicCodeList where branch does not contain UPDATED_BRANCH
        defaultBicCodeShouldBeFound("branch.doesNotContain=" + UPDATED_BRANCH);
    }


    @Test
    @Transactional
    public void getAllBicCodesByAddressIsEqualToSomething() throws Exception {
        // Initialize the database
        bicCodeRepository.saveAndFlush(bicCode);

        // Get all the bicCodeList where address equals to DEFAULT_ADDRESS
        defaultBicCodeShouldBeFound("address.equals=" + DEFAULT_ADDRESS);

        // Get all the bicCodeList where address equals to UPDATED_ADDRESS
        defaultBicCodeShouldNotBeFound("address.equals=" + UPDATED_ADDRESS);
    }

    @Test
    @Transactional
    public void getAllBicCodesByAddressIsNotEqualToSomething() throws Exception {
        // Initialize the database
        bicCodeRepository.saveAndFlush(bicCode);

        // Get all the bicCodeList where address not equals to DEFAULT_ADDRESS
        defaultBicCodeShouldNotBeFound("address.notEquals=" + DEFAULT_ADDRESS);

        // Get all the bicCodeList where address not equals to UPDATED_ADDRESS
        defaultBicCodeShouldBeFound("address.notEquals=" + UPDATED_ADDRESS);
    }

    @Test
    @Transactional
    public void getAllBicCodesByAddressIsInShouldWork() throws Exception {
        // Initialize the database
        bicCodeRepository.saveAndFlush(bicCode);

        // Get all the bicCodeList where address in DEFAULT_ADDRESS or UPDATED_ADDRESS
        defaultBicCodeShouldBeFound("address.in=" + DEFAULT_ADDRESS + "," + UPDATED_ADDRESS);

        // Get all the bicCodeList where address equals to UPDATED_ADDRESS
        defaultBicCodeShouldNotBeFound("address.in=" + UPDATED_ADDRESS);
    }

    @Test
    @Transactional
    public void getAllBicCodesByAddressIsNullOrNotNull() throws Exception {
        // Initialize the database
        bicCodeRepository.saveAndFlush(bicCode);

        // Get all the bicCodeList where address is not null
        defaultBicCodeShouldBeFound("address.specified=true");

        // Get all the bicCodeList where address is null
        defaultBicCodeShouldNotBeFound("address.specified=false");
    }
                @Test
    @Transactional
    public void getAllBicCodesByAddressContainsSomething() throws Exception {
        // Initialize the database
        bicCodeRepository.saveAndFlush(bicCode);

        // Get all the bicCodeList where address contains DEFAULT_ADDRESS
        defaultBicCodeShouldBeFound("address.contains=" + DEFAULT_ADDRESS);

        // Get all the bicCodeList where address contains UPDATED_ADDRESS
        defaultBicCodeShouldNotBeFound("address.contains=" + UPDATED_ADDRESS);
    }

    @Test
    @Transactional
    public void getAllBicCodesByAddressNotContainsSomething() throws Exception {
        // Initialize the database
        bicCodeRepository.saveAndFlush(bicCode);

        // Get all the bicCodeList where address does not contain DEFAULT_ADDRESS
        defaultBicCodeShouldNotBeFound("address.doesNotContain=" + DEFAULT_ADDRESS);

        // Get all the bicCodeList where address does not contain UPDATED_ADDRESS
        defaultBicCodeShouldBeFound("address.doesNotContain=" + UPDATED_ADDRESS);
    }


    @Test
    @Transactional
    public void getAllBicCodesByPostalCodeIsEqualToSomething() throws Exception {
        // Initialize the database
        bicCodeRepository.saveAndFlush(bicCode);

        // Get all the bicCodeList where postalCode equals to DEFAULT_POSTAL_CODE
        defaultBicCodeShouldBeFound("postalCode.equals=" + DEFAULT_POSTAL_CODE);

        // Get all the bicCodeList where postalCode equals to UPDATED_POSTAL_CODE
        defaultBicCodeShouldNotBeFound("postalCode.equals=" + UPDATED_POSTAL_CODE);
    }

    @Test
    @Transactional
    public void getAllBicCodesByPostalCodeIsNotEqualToSomething() throws Exception {
        // Initialize the database
        bicCodeRepository.saveAndFlush(bicCode);

        // Get all the bicCodeList where postalCode not equals to DEFAULT_POSTAL_CODE
        defaultBicCodeShouldNotBeFound("postalCode.notEquals=" + DEFAULT_POSTAL_CODE);

        // Get all the bicCodeList where postalCode not equals to UPDATED_POSTAL_CODE
        defaultBicCodeShouldBeFound("postalCode.notEquals=" + UPDATED_POSTAL_CODE);
    }

    @Test
    @Transactional
    public void getAllBicCodesByPostalCodeIsInShouldWork() throws Exception {
        // Initialize the database
        bicCodeRepository.saveAndFlush(bicCode);

        // Get all the bicCodeList where postalCode in DEFAULT_POSTAL_CODE or UPDATED_POSTAL_CODE
        defaultBicCodeShouldBeFound("postalCode.in=" + DEFAULT_POSTAL_CODE + "," + UPDATED_POSTAL_CODE);

        // Get all the bicCodeList where postalCode equals to UPDATED_POSTAL_CODE
        defaultBicCodeShouldNotBeFound("postalCode.in=" + UPDATED_POSTAL_CODE);
    }

    @Test
    @Transactional
    public void getAllBicCodesByPostalCodeIsNullOrNotNull() throws Exception {
        // Initialize the database
        bicCodeRepository.saveAndFlush(bicCode);

        // Get all the bicCodeList where postalCode is not null
        defaultBicCodeShouldBeFound("postalCode.specified=true");

        // Get all the bicCodeList where postalCode is null
        defaultBicCodeShouldNotBeFound("postalCode.specified=false");
    }
                @Test
    @Transactional
    public void getAllBicCodesByPostalCodeContainsSomething() throws Exception {
        // Initialize the database
        bicCodeRepository.saveAndFlush(bicCode);

        // Get all the bicCodeList where postalCode contains DEFAULT_POSTAL_CODE
        defaultBicCodeShouldBeFound("postalCode.contains=" + DEFAULT_POSTAL_CODE);

        // Get all the bicCodeList where postalCode contains UPDATED_POSTAL_CODE
        defaultBicCodeShouldNotBeFound("postalCode.contains=" + UPDATED_POSTAL_CODE);
    }

    @Test
    @Transactional
    public void getAllBicCodesByPostalCodeNotContainsSomething() throws Exception {
        // Initialize the database
        bicCodeRepository.saveAndFlush(bicCode);

        // Get all the bicCodeList where postalCode does not contain DEFAULT_POSTAL_CODE
        defaultBicCodeShouldNotBeFound("postalCode.doesNotContain=" + DEFAULT_POSTAL_CODE);

        // Get all the bicCodeList where postalCode does not contain UPDATED_POSTAL_CODE
        defaultBicCodeShouldBeFound("postalCode.doesNotContain=" + UPDATED_POSTAL_CODE);
    }


    @Test
    @Transactional
    public void getAllBicCodesByBicLatIsEqualToSomething() throws Exception {
        // Initialize the database
        bicCodeRepository.saveAndFlush(bicCode);

        // Get all the bicCodeList where bicLat equals to DEFAULT_BIC_LAT
        defaultBicCodeShouldBeFound("bicLat.equals=" + DEFAULT_BIC_LAT);

        // Get all the bicCodeList where bicLat equals to UPDATED_BIC_LAT
        defaultBicCodeShouldNotBeFound("bicLat.equals=" + UPDATED_BIC_LAT);
    }

    @Test
    @Transactional
    public void getAllBicCodesByBicLatIsNotEqualToSomething() throws Exception {
        // Initialize the database
        bicCodeRepository.saveAndFlush(bicCode);

        // Get all the bicCodeList where bicLat not equals to DEFAULT_BIC_LAT
        defaultBicCodeShouldNotBeFound("bicLat.notEquals=" + DEFAULT_BIC_LAT);

        // Get all the bicCodeList where bicLat not equals to UPDATED_BIC_LAT
        defaultBicCodeShouldBeFound("bicLat.notEquals=" + UPDATED_BIC_LAT);
    }

    @Test
    @Transactional
    public void getAllBicCodesByBicLatIsInShouldWork() throws Exception {
        // Initialize the database
        bicCodeRepository.saveAndFlush(bicCode);

        // Get all the bicCodeList where bicLat in DEFAULT_BIC_LAT or UPDATED_BIC_LAT
        defaultBicCodeShouldBeFound("bicLat.in=" + DEFAULT_BIC_LAT + "," + UPDATED_BIC_LAT);

        // Get all the bicCodeList where bicLat equals to UPDATED_BIC_LAT
        defaultBicCodeShouldNotBeFound("bicLat.in=" + UPDATED_BIC_LAT);
    }

    @Test
    @Transactional
    public void getAllBicCodesByBicLatIsNullOrNotNull() throws Exception {
        // Initialize the database
        bicCodeRepository.saveAndFlush(bicCode);

        // Get all the bicCodeList where bicLat is not null
        defaultBicCodeShouldBeFound("bicLat.specified=true");

        // Get all the bicCodeList where bicLat is null
        defaultBicCodeShouldNotBeFound("bicLat.specified=false");
    }

    @Test
    @Transactional
    public void getAllBicCodesByBicLatIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        bicCodeRepository.saveAndFlush(bicCode);

        // Get all the bicCodeList where bicLat is greater than or equal to DEFAULT_BIC_LAT
        defaultBicCodeShouldBeFound("bicLat.greaterThanOrEqual=" + DEFAULT_BIC_LAT);

        // Get all the bicCodeList where bicLat is greater than or equal to UPDATED_BIC_LAT
        defaultBicCodeShouldNotBeFound("bicLat.greaterThanOrEqual=" + UPDATED_BIC_LAT);
    }

    @Test
    @Transactional
    public void getAllBicCodesByBicLatIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        bicCodeRepository.saveAndFlush(bicCode);

        // Get all the bicCodeList where bicLat is less than or equal to DEFAULT_BIC_LAT
        defaultBicCodeShouldBeFound("bicLat.lessThanOrEqual=" + DEFAULT_BIC_LAT);

        // Get all the bicCodeList where bicLat is less than or equal to SMALLER_BIC_LAT
        defaultBicCodeShouldNotBeFound("bicLat.lessThanOrEqual=" + SMALLER_BIC_LAT);
    }

    @Test
    @Transactional
    public void getAllBicCodesByBicLatIsLessThanSomething() throws Exception {
        // Initialize the database
        bicCodeRepository.saveAndFlush(bicCode);

        // Get all the bicCodeList where bicLat is less than DEFAULT_BIC_LAT
        defaultBicCodeShouldNotBeFound("bicLat.lessThan=" + DEFAULT_BIC_LAT);

        // Get all the bicCodeList where bicLat is less than UPDATED_BIC_LAT
        defaultBicCodeShouldBeFound("bicLat.lessThan=" + UPDATED_BIC_LAT);
    }

    @Test
    @Transactional
    public void getAllBicCodesByBicLatIsGreaterThanSomething() throws Exception {
        // Initialize the database
        bicCodeRepository.saveAndFlush(bicCode);

        // Get all the bicCodeList where bicLat is greater than DEFAULT_BIC_LAT
        defaultBicCodeShouldNotBeFound("bicLat.greaterThan=" + DEFAULT_BIC_LAT);

        // Get all the bicCodeList where bicLat is greater than SMALLER_BIC_LAT
        defaultBicCodeShouldBeFound("bicLat.greaterThan=" + SMALLER_BIC_LAT);
    }


    @Test
    @Transactional
    public void getAllBicCodesByBicLngIsEqualToSomething() throws Exception {
        // Initialize the database
        bicCodeRepository.saveAndFlush(bicCode);

        // Get all the bicCodeList where bicLng equals to DEFAULT_BIC_LNG
        defaultBicCodeShouldBeFound("bicLng.equals=" + DEFAULT_BIC_LNG);

        // Get all the bicCodeList where bicLng equals to UPDATED_BIC_LNG
        defaultBicCodeShouldNotBeFound("bicLng.equals=" + UPDATED_BIC_LNG);
    }

    @Test
    @Transactional
    public void getAllBicCodesByBicLngIsNotEqualToSomething() throws Exception {
        // Initialize the database
        bicCodeRepository.saveAndFlush(bicCode);

        // Get all the bicCodeList where bicLng not equals to DEFAULT_BIC_LNG
        defaultBicCodeShouldNotBeFound("bicLng.notEquals=" + DEFAULT_BIC_LNG);

        // Get all the bicCodeList where bicLng not equals to UPDATED_BIC_LNG
        defaultBicCodeShouldBeFound("bicLng.notEquals=" + UPDATED_BIC_LNG);
    }

    @Test
    @Transactional
    public void getAllBicCodesByBicLngIsInShouldWork() throws Exception {
        // Initialize the database
        bicCodeRepository.saveAndFlush(bicCode);

        // Get all the bicCodeList where bicLng in DEFAULT_BIC_LNG or UPDATED_BIC_LNG
        defaultBicCodeShouldBeFound("bicLng.in=" + DEFAULT_BIC_LNG + "," + UPDATED_BIC_LNG);

        // Get all the bicCodeList where bicLng equals to UPDATED_BIC_LNG
        defaultBicCodeShouldNotBeFound("bicLng.in=" + UPDATED_BIC_LNG);
    }

    @Test
    @Transactional
    public void getAllBicCodesByBicLngIsNullOrNotNull() throws Exception {
        // Initialize the database
        bicCodeRepository.saveAndFlush(bicCode);

        // Get all the bicCodeList where bicLng is not null
        defaultBicCodeShouldBeFound("bicLng.specified=true");

        // Get all the bicCodeList where bicLng is null
        defaultBicCodeShouldNotBeFound("bicLng.specified=false");
    }

    @Test
    @Transactional
    public void getAllBicCodesByBicLngIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        bicCodeRepository.saveAndFlush(bicCode);

        // Get all the bicCodeList where bicLng is greater than or equal to DEFAULT_BIC_LNG
        defaultBicCodeShouldBeFound("bicLng.greaterThanOrEqual=" + DEFAULT_BIC_LNG);

        // Get all the bicCodeList where bicLng is greater than or equal to UPDATED_BIC_LNG
        defaultBicCodeShouldNotBeFound("bicLng.greaterThanOrEqual=" + UPDATED_BIC_LNG);
    }

    @Test
    @Transactional
    public void getAllBicCodesByBicLngIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        bicCodeRepository.saveAndFlush(bicCode);

        // Get all the bicCodeList where bicLng is less than or equal to DEFAULT_BIC_LNG
        defaultBicCodeShouldBeFound("bicLng.lessThanOrEqual=" + DEFAULT_BIC_LNG);

        // Get all the bicCodeList where bicLng is less than or equal to SMALLER_BIC_LNG
        defaultBicCodeShouldNotBeFound("bicLng.lessThanOrEqual=" + SMALLER_BIC_LNG);
    }

    @Test
    @Transactional
    public void getAllBicCodesByBicLngIsLessThanSomething() throws Exception {
        // Initialize the database
        bicCodeRepository.saveAndFlush(bicCode);

        // Get all the bicCodeList where bicLng is less than DEFAULT_BIC_LNG
        defaultBicCodeShouldNotBeFound("bicLng.lessThan=" + DEFAULT_BIC_LNG);

        // Get all the bicCodeList where bicLng is less than UPDATED_BIC_LNG
        defaultBicCodeShouldBeFound("bicLng.lessThan=" + UPDATED_BIC_LNG);
    }

    @Test
    @Transactional
    public void getAllBicCodesByBicLngIsGreaterThanSomething() throws Exception {
        // Initialize the database
        bicCodeRepository.saveAndFlush(bicCode);

        // Get all the bicCodeList where bicLng is greater than DEFAULT_BIC_LNG
        defaultBicCodeShouldNotBeFound("bicLng.greaterThan=" + DEFAULT_BIC_LNG);

        // Get all the bicCodeList where bicLng is greater than SMALLER_BIC_LNG
        defaultBicCodeShouldBeFound("bicLng.greaterThan=" + SMALLER_BIC_LNG);
    }


    @Test
    @Transactional
    public void getAllBicCodesByCountryIsEqualToSomething() throws Exception {
        // Initialize the database
        bicCodeRepository.saveAndFlush(bicCode);
        Country country = CountryStubFactory.createEntity();
        em.persist(country);
        em.flush();
        bicCode.setCountry(country);
        bicCodeRepository.saveAndFlush(bicCode);
        Long countryId = country.getId();

        // Get all the bicCodeList where country equals to countryId
        defaultBicCodeShouldBeFound("countryId.equals=" + countryId);

        // Get all the bicCodeList where country equals to countryId + 1
        defaultBicCodeShouldNotBeFound("countryId.equals=" + (countryId + 1));
    }


    @Test
    @Transactional
    public void getAllBicCodesByFinancialInstitutionIsEqualToSomething() throws Exception {
        // Initialize the database
        bicCodeRepository.saveAndFlush(bicCode);
        FinancialInstitution financialInstitution = FinancialInstitutionResourceIT.createEntity(em);
        em.persist(financialInstitution);
        em.flush();
        bicCode.setFinancialInstitution(financialInstitution);
        bicCodeRepository.saveAndFlush(bicCode);
        Long financialInstitutionId = financialInstitution.getId();

        // Get all the bicCodeList where financialInstitution equals to financialInstitutionId
        defaultBicCodeShouldBeFound("financialInstitutionId.equals=" + financialInstitutionId);

        // Get all the bicCodeList where financialInstitution equals to financialInstitutionId + 1
        defaultBicCodeShouldNotBeFound("financialInstitutionId.equals=" + (financialInstitutionId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultBicCodeShouldBeFound(String filter) throws Exception {
        restBicCodeMockMvc.perform(get("/api/bic-codes?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(bicCode.getId().intValue())))
            .andExpect(jsonPath("$.[*].swiftCode").value(hasItem(DEFAULT_SWIFT_CODE)))
            .andExpect(jsonPath("$.[*].city").value(hasItem(DEFAULT_CITY)))
            .andExpect(jsonPath("$.[*].branch").value(hasItem(DEFAULT_BRANCH)))
            .andExpect(jsonPath("$.[*].address").value(hasItem(DEFAULT_ADDRESS)))
            .andExpect(jsonPath("$.[*].postalCode").value(hasItem(DEFAULT_POSTAL_CODE)))
            .andExpect(jsonPath("$.[*].bicLat").value(hasItem(DEFAULT_BIC_LAT.doubleValue())))
            .andExpect(jsonPath("$.[*].bicLng").value(hasItem(DEFAULT_BIC_LNG.doubleValue())));

        // Check, that the count call also returns 1
        restBicCodeMockMvc.perform(get("/api/bic-codes/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultBicCodeShouldNotBeFound(String filter) throws Exception {
        restBicCodeMockMvc.perform(get("/api/bic-codes?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restBicCodeMockMvc.perform(get("/api/bic-codes/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    public void getNonExistingBicCode() throws Exception {
        // Get the bicCode
        restBicCodeMockMvc.perform(get("/api/bic-codes/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateBicCode() throws Exception {
        // Initialize the database
        bicCodeService.save(bicCode);

        int databaseSizeBeforeUpdate = bicCodeRepository.findAll().size();

        // Update the bicCode
        BicCode updatedBicCode = bicCodeRepository.findById(bicCode.getId()).get();
        // Disconnect from session so that the updates on updatedBicCode are not directly saved in db
        em.detach(updatedBicCode);
        updatedBicCode
            .swiftCode(UPDATED_SWIFT_CODE)
            .city(UPDATED_CITY)
            .branch(UPDATED_BRANCH)
            .address(UPDATED_ADDRESS)
            .postalCode(UPDATED_POSTAL_CODE)
            .bicLat(UPDATED_BIC_LAT)
            .bicLng(UPDATED_BIC_LNG);

        restBicCodeMockMvc.perform(put("/api/bic-codes")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedBicCode)))
            .andExpect(status().isOk());

        // Validate the BicCode in the database
        List<BicCode> bicCodeList = bicCodeRepository.findAll();
        assertThat(bicCodeList).hasSize(databaseSizeBeforeUpdate);
        BicCode testBicCode = bicCodeList.get(bicCodeList.size() - 1);
        assertThat(testBicCode.getSwiftCode()).isEqualTo(UPDATED_SWIFT_CODE);
        assertThat(testBicCode.getCity()).isEqualTo(UPDATED_CITY);
        assertThat(testBicCode.getBranch()).isEqualTo(UPDATED_BRANCH);
        assertThat(testBicCode.getAddress()).isEqualTo(UPDATED_ADDRESS);
        assertThat(testBicCode.getPostalCode()).isEqualTo(UPDATED_POSTAL_CODE);
        assertThat(testBicCode.getBicLat()).isEqualTo(UPDATED_BIC_LAT);
        assertThat(testBicCode.getBicLng()).isEqualTo(UPDATED_BIC_LNG);
    }

    @Test
    @Transactional
    public void updateNonExistingBicCode() throws Exception {
        int databaseSizeBeforeUpdate = bicCodeRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restBicCodeMockMvc.perform(put("/api/bic-codes")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(bicCode)))
            .andExpect(status().isBadRequest());

        // Validate the BicCode in the database
        List<BicCode> bicCodeList = bicCodeRepository.findAll();
        assertThat(bicCodeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteBicCode() throws Exception {
        // Initialize the database
        bicCodeService.save(bicCode);

        int databaseSizeBeforeDelete = bicCodeRepository.findAll().size();

        // Delete the bicCode
        restBicCodeMockMvc.perform(delete("/api/bic-codes/{id}", bicCode.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<BicCode> bicCodeList = bicCodeRepository.findAll();
        assertThat(bicCodeList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
