package com.ag04.fidata.web.rest;

import com.ag04.fidata.domain.Country;


public class CountryStubFactory {
    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_CODE = "AAAAAAAAAA";
    private static final String UPDATED_CODE = "BBBBBBBBBB";

    private static final String DEFAULT_CODE_A_2 = "AA";
    private static final String UPDATED_CODE_A_2 = "BB";

    private static final String DEFAULT_CODE_A_3 = "AAA";
    private static final String UPDATED_CODE_A_3 = "BBB";

    private static final String DEFAULT_FLAG = "AAAAAAAAAA";
    private static final String UPDATED_FLAG = "BBBBBBBBBB";

    private static final Boolean DEFAULT_ACTIVE = false;
    private static final Boolean UPDATED_ACTIVE = true;

    public static Country createEntity() {
        Country country = new Country()
            .name(DEFAULT_NAME)
            .code(DEFAULT_CODE)
            .codeA2(DEFAULT_CODE_A_2)
            .codeA3(DEFAULT_CODE_A_3)
            .flag(DEFAULT_FLAG)
            .active(DEFAULT_ACTIVE);
        return country;
    }
}
