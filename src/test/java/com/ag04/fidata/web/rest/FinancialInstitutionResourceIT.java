package com.ag04.fidata.web.rest;

import com.ag04.fidata.FidataServiceApp;
import com.ag04.fidata.domain.FinancialInstitution;
import com.ag04.fidata.domain.BicCode;
import com.ag04.fidata.domain.Country;
import com.ag04.fidata.repository.FinancialInstitutionRepository;
import com.ag04.fidata.service.FinancialInstitutionService;
import com.ag04.fidata.service.dto.FinancialInstitutionCriteria;
import com.ag04.fidata.service.FinancialInstitutionQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link FinancialInstitutionResource} REST controller.
 */
@SpringBootTest(classes = FidataServiceApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class FinancialInstitutionResourceIT {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    private static final String DEFAULT_ADDRESS = "AAAAAAAAAA";
    private static final String UPDATED_ADDRESS = "BBBBBBBBBB";

    private static final String DEFAULT_ADDRESS_2 = "AAAAAAAAAA";
    private static final String UPDATED_ADDRESS_2 = "BBBBBBBBBB";

    private static final String DEFAULT_CITY = "AAAAAAAAAA";
    private static final String UPDATED_CITY = "BBBBBBBBBB";

    private static final String DEFAULT_STATE = "AAAAAAAAAA";
    private static final String UPDATED_STATE = "BBBBBBBBBB";

    private static final String DEFAULT_POSTAL_CODE = "AAAAAAAAAA";
    private static final String UPDATED_POSTAL_CODE = "BBBBBBBBBB";

    @Autowired
    private FinancialInstitutionRepository financialInstitutionRepository;

    @Autowired
    private FinancialInstitutionService financialInstitutionService;

    @Autowired
    private FinancialInstitutionQueryService financialInstitutionQueryService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restFinancialInstitutionMockMvc;

    private FinancialInstitution financialInstitution;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static FinancialInstitution createEntity(EntityManager em) {
        FinancialInstitution financialInstitution = new FinancialInstitution()
            .name(DEFAULT_NAME)
            .description(DEFAULT_DESCRIPTION)
            .address(DEFAULT_ADDRESS)
            .address2(DEFAULT_ADDRESS_2)
            .city(DEFAULT_CITY)
            .state(DEFAULT_STATE)
            .postalCode(DEFAULT_POSTAL_CODE);
        return financialInstitution;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static FinancialInstitution createUpdatedEntity(EntityManager em) {
        FinancialInstitution financialInstitution = new FinancialInstitution()
            .name(UPDATED_NAME)
            .description(UPDATED_DESCRIPTION)
            .address(UPDATED_ADDRESS)
            .address2(UPDATED_ADDRESS_2)
            .city(UPDATED_CITY)
            .state(UPDATED_STATE)
            .postalCode(UPDATED_POSTAL_CODE);
        return financialInstitution;
    }

    @BeforeEach
    public void initTest() {
        financialInstitution = createEntity(em);
    }

    @Test
    @Transactional
    public void createFinancialInstitution() throws Exception {
        int databaseSizeBeforeCreate = financialInstitutionRepository.findAll().size();
        // Create the FinancialInstitution
        restFinancialInstitutionMockMvc.perform(post("/api/financial-institutions")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(financialInstitution)))
            .andExpect(status().isCreated());

        // Validate the FinancialInstitution in the database
        List<FinancialInstitution> financialInstitutionList = financialInstitutionRepository.findAll();
        assertThat(financialInstitutionList).hasSize(databaseSizeBeforeCreate + 1);
        FinancialInstitution testFinancialInstitution = financialInstitutionList.get(financialInstitutionList.size() - 1);
        assertThat(testFinancialInstitution.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testFinancialInstitution.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testFinancialInstitution.getAddress()).isEqualTo(DEFAULT_ADDRESS);
        assertThat(testFinancialInstitution.getAddress2()).isEqualTo(DEFAULT_ADDRESS_2);
        assertThat(testFinancialInstitution.getCity()).isEqualTo(DEFAULT_CITY);
        assertThat(testFinancialInstitution.getState()).isEqualTo(DEFAULT_STATE);
        assertThat(testFinancialInstitution.getPostalCode()).isEqualTo(DEFAULT_POSTAL_CODE);
    }

    @Test
    @Transactional
    public void createFinancialInstitutionWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = financialInstitutionRepository.findAll().size();

        // Create the FinancialInstitution with an existing ID
        financialInstitution.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restFinancialInstitutionMockMvc.perform(post("/api/financial-institutions")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(financialInstitution)))
            .andExpect(status().isBadRequest());

        // Validate the FinancialInstitution in the database
        List<FinancialInstitution> financialInstitutionList = financialInstitutionRepository.findAll();
        assertThat(financialInstitutionList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = financialInstitutionRepository.findAll().size();
        // set the field null
        financialInstitution.setName(null);

        // Create the FinancialInstitution, which fails.


        restFinancialInstitutionMockMvc.perform(post("/api/financial-institutions")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(financialInstitution)))
            .andExpect(status().isBadRequest());

        List<FinancialInstitution> financialInstitutionList = financialInstitutionRepository.findAll();
        assertThat(financialInstitutionList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllFinancialInstitutions() throws Exception {
        // Initialize the database
        financialInstitutionRepository.saveAndFlush(financialInstitution);

        // Get all the financialInstitutionList
        restFinancialInstitutionMockMvc.perform(get("/api/financial-institutions?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(financialInstitution.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION)))
            .andExpect(jsonPath("$.[*].address").value(hasItem(DEFAULT_ADDRESS)))
            .andExpect(jsonPath("$.[*].address2").value(hasItem(DEFAULT_ADDRESS_2)))
            .andExpect(jsonPath("$.[*].city").value(hasItem(DEFAULT_CITY)))
            .andExpect(jsonPath("$.[*].state").value(hasItem(DEFAULT_STATE)))
            .andExpect(jsonPath("$.[*].postalCode").value(hasItem(DEFAULT_POSTAL_CODE)));
    }

    @Test
    @Transactional
    public void getFinancialInstitution() throws Exception {
        // Initialize the database
        financialInstitutionRepository.saveAndFlush(financialInstitution);

        // Get the financialInstitution
        restFinancialInstitutionMockMvc.perform(get("/api/financial-institutions/{id}", financialInstitution.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(financialInstitution.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION))
            .andExpect(jsonPath("$.address").value(DEFAULT_ADDRESS))
            .andExpect(jsonPath("$.address2").value(DEFAULT_ADDRESS_2))
            .andExpect(jsonPath("$.city").value(DEFAULT_CITY))
            .andExpect(jsonPath("$.state").value(DEFAULT_STATE))
            .andExpect(jsonPath("$.postalCode").value(DEFAULT_POSTAL_CODE));
    }


    @Test
    @Transactional
    public void getFinancialInstitutionsByIdFiltering() throws Exception {
        // Initialize the database
        financialInstitutionRepository.saveAndFlush(financialInstitution);

        Long id = financialInstitution.getId();

        defaultFinancialInstitutionShouldBeFound("id.equals=" + id);
        defaultFinancialInstitutionShouldNotBeFound("id.notEquals=" + id);

        defaultFinancialInstitutionShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultFinancialInstitutionShouldNotBeFound("id.greaterThan=" + id);

        defaultFinancialInstitutionShouldBeFound("id.lessThanOrEqual=" + id);
        defaultFinancialInstitutionShouldNotBeFound("id.lessThan=" + id);
    }


    @Test
    @Transactional
    public void getAllFinancialInstitutionsByNameIsEqualToSomething() throws Exception {
        // Initialize the database
        financialInstitutionRepository.saveAndFlush(financialInstitution);

        // Get all the financialInstitutionList where name equals to DEFAULT_NAME
        defaultFinancialInstitutionShouldBeFound("name.equals=" + DEFAULT_NAME);

        // Get all the financialInstitutionList where name equals to UPDATED_NAME
        defaultFinancialInstitutionShouldNotBeFound("name.equals=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    public void getAllFinancialInstitutionsByNameIsNotEqualToSomething() throws Exception {
        // Initialize the database
        financialInstitutionRepository.saveAndFlush(financialInstitution);

        // Get all the financialInstitutionList where name not equals to DEFAULT_NAME
        defaultFinancialInstitutionShouldNotBeFound("name.notEquals=" + DEFAULT_NAME);

        // Get all the financialInstitutionList where name not equals to UPDATED_NAME
        defaultFinancialInstitutionShouldBeFound("name.notEquals=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    public void getAllFinancialInstitutionsByNameIsInShouldWork() throws Exception {
        // Initialize the database
        financialInstitutionRepository.saveAndFlush(financialInstitution);

        // Get all the financialInstitutionList where name in DEFAULT_NAME or UPDATED_NAME
        defaultFinancialInstitutionShouldBeFound("name.in=" + DEFAULT_NAME + "," + UPDATED_NAME);

        // Get all the financialInstitutionList where name equals to UPDATED_NAME
        defaultFinancialInstitutionShouldNotBeFound("name.in=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    public void getAllFinancialInstitutionsByNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        financialInstitutionRepository.saveAndFlush(financialInstitution);

        // Get all the financialInstitutionList where name is not null
        defaultFinancialInstitutionShouldBeFound("name.specified=true");

        // Get all the financialInstitutionList where name is null
        defaultFinancialInstitutionShouldNotBeFound("name.specified=false");
    }
                @Test
    @Transactional
    public void getAllFinancialInstitutionsByNameContainsSomething() throws Exception {
        // Initialize the database
        financialInstitutionRepository.saveAndFlush(financialInstitution);

        // Get all the financialInstitutionList where name contains DEFAULT_NAME
        defaultFinancialInstitutionShouldBeFound("name.contains=" + DEFAULT_NAME);

        // Get all the financialInstitutionList where name contains UPDATED_NAME
        defaultFinancialInstitutionShouldNotBeFound("name.contains=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    public void getAllFinancialInstitutionsByNameNotContainsSomething() throws Exception {
        // Initialize the database
        financialInstitutionRepository.saveAndFlush(financialInstitution);

        // Get all the financialInstitutionList where name does not contain DEFAULT_NAME
        defaultFinancialInstitutionShouldNotBeFound("name.doesNotContain=" + DEFAULT_NAME);

        // Get all the financialInstitutionList where name does not contain UPDATED_NAME
        defaultFinancialInstitutionShouldBeFound("name.doesNotContain=" + UPDATED_NAME);
    }


    @Test
    @Transactional
    public void getAllFinancialInstitutionsByDescriptionIsEqualToSomething() throws Exception {
        // Initialize the database
        financialInstitutionRepository.saveAndFlush(financialInstitution);

        // Get all the financialInstitutionList where description equals to DEFAULT_DESCRIPTION
        defaultFinancialInstitutionShouldBeFound("description.equals=" + DEFAULT_DESCRIPTION);

        // Get all the financialInstitutionList where description equals to UPDATED_DESCRIPTION
        defaultFinancialInstitutionShouldNotBeFound("description.equals=" + UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    public void getAllFinancialInstitutionsByDescriptionIsNotEqualToSomething() throws Exception {
        // Initialize the database
        financialInstitutionRepository.saveAndFlush(financialInstitution);

        // Get all the financialInstitutionList where description not equals to DEFAULT_DESCRIPTION
        defaultFinancialInstitutionShouldNotBeFound("description.notEquals=" + DEFAULT_DESCRIPTION);

        // Get all the financialInstitutionList where description not equals to UPDATED_DESCRIPTION
        defaultFinancialInstitutionShouldBeFound("description.notEquals=" + UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    public void getAllFinancialInstitutionsByDescriptionIsInShouldWork() throws Exception {
        // Initialize the database
        financialInstitutionRepository.saveAndFlush(financialInstitution);

        // Get all the financialInstitutionList where description in DEFAULT_DESCRIPTION or UPDATED_DESCRIPTION
        defaultFinancialInstitutionShouldBeFound("description.in=" + DEFAULT_DESCRIPTION + "," + UPDATED_DESCRIPTION);

        // Get all the financialInstitutionList where description equals to UPDATED_DESCRIPTION
        defaultFinancialInstitutionShouldNotBeFound("description.in=" + UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    public void getAllFinancialInstitutionsByDescriptionIsNullOrNotNull() throws Exception {
        // Initialize the database
        financialInstitutionRepository.saveAndFlush(financialInstitution);

        // Get all the financialInstitutionList where description is not null
        defaultFinancialInstitutionShouldBeFound("description.specified=true");

        // Get all the financialInstitutionList where description is null
        defaultFinancialInstitutionShouldNotBeFound("description.specified=false");
    }
                @Test
    @Transactional
    public void getAllFinancialInstitutionsByDescriptionContainsSomething() throws Exception {
        // Initialize the database
        financialInstitutionRepository.saveAndFlush(financialInstitution);

        // Get all the financialInstitutionList where description contains DEFAULT_DESCRIPTION
        defaultFinancialInstitutionShouldBeFound("description.contains=" + DEFAULT_DESCRIPTION);

        // Get all the financialInstitutionList where description contains UPDATED_DESCRIPTION
        defaultFinancialInstitutionShouldNotBeFound("description.contains=" + UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    public void getAllFinancialInstitutionsByDescriptionNotContainsSomething() throws Exception {
        // Initialize the database
        financialInstitutionRepository.saveAndFlush(financialInstitution);

        // Get all the financialInstitutionList where description does not contain DEFAULT_DESCRIPTION
        defaultFinancialInstitutionShouldNotBeFound("description.doesNotContain=" + DEFAULT_DESCRIPTION);

        // Get all the financialInstitutionList where description does not contain UPDATED_DESCRIPTION
        defaultFinancialInstitutionShouldBeFound("description.doesNotContain=" + UPDATED_DESCRIPTION);
    }


    @Test
    @Transactional
    public void getAllFinancialInstitutionsByAddressIsEqualToSomething() throws Exception {
        // Initialize the database
        financialInstitutionRepository.saveAndFlush(financialInstitution);

        // Get all the financialInstitutionList where address equals to DEFAULT_ADDRESS
        defaultFinancialInstitutionShouldBeFound("address.equals=" + DEFAULT_ADDRESS);

        // Get all the financialInstitutionList where address equals to UPDATED_ADDRESS
        defaultFinancialInstitutionShouldNotBeFound("address.equals=" + UPDATED_ADDRESS);
    }

    @Test
    @Transactional
    public void getAllFinancialInstitutionsByAddressIsNotEqualToSomething() throws Exception {
        // Initialize the database
        financialInstitutionRepository.saveAndFlush(financialInstitution);

        // Get all the financialInstitutionList where address not equals to DEFAULT_ADDRESS
        defaultFinancialInstitutionShouldNotBeFound("address.notEquals=" + DEFAULT_ADDRESS);

        // Get all the financialInstitutionList where address not equals to UPDATED_ADDRESS
        defaultFinancialInstitutionShouldBeFound("address.notEquals=" + UPDATED_ADDRESS);
    }

    @Test
    @Transactional
    public void getAllFinancialInstitutionsByAddressIsInShouldWork() throws Exception {
        // Initialize the database
        financialInstitutionRepository.saveAndFlush(financialInstitution);

        // Get all the financialInstitutionList where address in DEFAULT_ADDRESS or UPDATED_ADDRESS
        defaultFinancialInstitutionShouldBeFound("address.in=" + DEFAULT_ADDRESS + "," + UPDATED_ADDRESS);

        // Get all the financialInstitutionList where address equals to UPDATED_ADDRESS
        defaultFinancialInstitutionShouldNotBeFound("address.in=" + UPDATED_ADDRESS);
    }

    @Test
    @Transactional
    public void getAllFinancialInstitutionsByAddressIsNullOrNotNull() throws Exception {
        // Initialize the database
        financialInstitutionRepository.saveAndFlush(financialInstitution);

        // Get all the financialInstitutionList where address is not null
        defaultFinancialInstitutionShouldBeFound("address.specified=true");

        // Get all the financialInstitutionList where address is null
        defaultFinancialInstitutionShouldNotBeFound("address.specified=false");
    }
                @Test
    @Transactional
    public void getAllFinancialInstitutionsByAddressContainsSomething() throws Exception {
        // Initialize the database
        financialInstitutionRepository.saveAndFlush(financialInstitution);

        // Get all the financialInstitutionList where address contains DEFAULT_ADDRESS
        defaultFinancialInstitutionShouldBeFound("address.contains=" + DEFAULT_ADDRESS);

        // Get all the financialInstitutionList where address contains UPDATED_ADDRESS
        defaultFinancialInstitutionShouldNotBeFound("address.contains=" + UPDATED_ADDRESS);
    }

    @Test
    @Transactional
    public void getAllFinancialInstitutionsByAddressNotContainsSomething() throws Exception {
        // Initialize the database
        financialInstitutionRepository.saveAndFlush(financialInstitution);

        // Get all the financialInstitutionList where address does not contain DEFAULT_ADDRESS
        defaultFinancialInstitutionShouldNotBeFound("address.doesNotContain=" + DEFAULT_ADDRESS);

        // Get all the financialInstitutionList where address does not contain UPDATED_ADDRESS
        defaultFinancialInstitutionShouldBeFound("address.doesNotContain=" + UPDATED_ADDRESS);
    }


    @Test
    @Transactional
    public void getAllFinancialInstitutionsByAddress2IsEqualToSomething() throws Exception {
        // Initialize the database
        financialInstitutionRepository.saveAndFlush(financialInstitution);

        // Get all the financialInstitutionList where address2 equals to DEFAULT_ADDRESS_2
        defaultFinancialInstitutionShouldBeFound("address2.equals=" + DEFAULT_ADDRESS_2);

        // Get all the financialInstitutionList where address2 equals to UPDATED_ADDRESS_2
        defaultFinancialInstitutionShouldNotBeFound("address2.equals=" + UPDATED_ADDRESS_2);
    }

    @Test
    @Transactional
    public void getAllFinancialInstitutionsByAddress2IsNotEqualToSomething() throws Exception {
        // Initialize the database
        financialInstitutionRepository.saveAndFlush(financialInstitution);

        // Get all the financialInstitutionList where address2 not equals to DEFAULT_ADDRESS_2
        defaultFinancialInstitutionShouldNotBeFound("address2.notEquals=" + DEFAULT_ADDRESS_2);

        // Get all the financialInstitutionList where address2 not equals to UPDATED_ADDRESS_2
        defaultFinancialInstitutionShouldBeFound("address2.notEquals=" + UPDATED_ADDRESS_2);
    }

    @Test
    @Transactional
    public void getAllFinancialInstitutionsByAddress2IsInShouldWork() throws Exception {
        // Initialize the database
        financialInstitutionRepository.saveAndFlush(financialInstitution);

        // Get all the financialInstitutionList where address2 in DEFAULT_ADDRESS_2 or UPDATED_ADDRESS_2
        defaultFinancialInstitutionShouldBeFound("address2.in=" + DEFAULT_ADDRESS_2 + "," + UPDATED_ADDRESS_2);

        // Get all the financialInstitutionList where address2 equals to UPDATED_ADDRESS_2
        defaultFinancialInstitutionShouldNotBeFound("address2.in=" + UPDATED_ADDRESS_2);
    }

    @Test
    @Transactional
    public void getAllFinancialInstitutionsByAddress2IsNullOrNotNull() throws Exception {
        // Initialize the database
        financialInstitutionRepository.saveAndFlush(financialInstitution);

        // Get all the financialInstitutionList where address2 is not null
        defaultFinancialInstitutionShouldBeFound("address2.specified=true");

        // Get all the financialInstitutionList where address2 is null
        defaultFinancialInstitutionShouldNotBeFound("address2.specified=false");
    }
                @Test
    @Transactional
    public void getAllFinancialInstitutionsByAddress2ContainsSomething() throws Exception {
        // Initialize the database
        financialInstitutionRepository.saveAndFlush(financialInstitution);

        // Get all the financialInstitutionList where address2 contains DEFAULT_ADDRESS_2
        defaultFinancialInstitutionShouldBeFound("address2.contains=" + DEFAULT_ADDRESS_2);

        // Get all the financialInstitutionList where address2 contains UPDATED_ADDRESS_2
        defaultFinancialInstitutionShouldNotBeFound("address2.contains=" + UPDATED_ADDRESS_2);
    }

    @Test
    @Transactional
    public void getAllFinancialInstitutionsByAddress2NotContainsSomething() throws Exception {
        // Initialize the database
        financialInstitutionRepository.saveAndFlush(financialInstitution);

        // Get all the financialInstitutionList where address2 does not contain DEFAULT_ADDRESS_2
        defaultFinancialInstitutionShouldNotBeFound("address2.doesNotContain=" + DEFAULT_ADDRESS_2);

        // Get all the financialInstitutionList where address2 does not contain UPDATED_ADDRESS_2
        defaultFinancialInstitutionShouldBeFound("address2.doesNotContain=" + UPDATED_ADDRESS_2);
    }


    @Test
    @Transactional
    public void getAllFinancialInstitutionsByCityIsEqualToSomething() throws Exception {
        // Initialize the database
        financialInstitutionRepository.saveAndFlush(financialInstitution);

        // Get all the financialInstitutionList where city equals to DEFAULT_CITY
        defaultFinancialInstitutionShouldBeFound("city.equals=" + DEFAULT_CITY);

        // Get all the financialInstitutionList where city equals to UPDATED_CITY
        defaultFinancialInstitutionShouldNotBeFound("city.equals=" + UPDATED_CITY);
    }

    @Test
    @Transactional
    public void getAllFinancialInstitutionsByCityIsNotEqualToSomething() throws Exception {
        // Initialize the database
        financialInstitutionRepository.saveAndFlush(financialInstitution);

        // Get all the financialInstitutionList where city not equals to DEFAULT_CITY
        defaultFinancialInstitutionShouldNotBeFound("city.notEquals=" + DEFAULT_CITY);

        // Get all the financialInstitutionList where city not equals to UPDATED_CITY
        defaultFinancialInstitutionShouldBeFound("city.notEquals=" + UPDATED_CITY);
    }

    @Test
    @Transactional
    public void getAllFinancialInstitutionsByCityIsInShouldWork() throws Exception {
        // Initialize the database
        financialInstitutionRepository.saveAndFlush(financialInstitution);

        // Get all the financialInstitutionList where city in DEFAULT_CITY or UPDATED_CITY
        defaultFinancialInstitutionShouldBeFound("city.in=" + DEFAULT_CITY + "," + UPDATED_CITY);

        // Get all the financialInstitutionList where city equals to UPDATED_CITY
        defaultFinancialInstitutionShouldNotBeFound("city.in=" + UPDATED_CITY);
    }

    @Test
    @Transactional
    public void getAllFinancialInstitutionsByCityIsNullOrNotNull() throws Exception {
        // Initialize the database
        financialInstitutionRepository.saveAndFlush(financialInstitution);

        // Get all the financialInstitutionList where city is not null
        defaultFinancialInstitutionShouldBeFound("city.specified=true");

        // Get all the financialInstitutionList where city is null
        defaultFinancialInstitutionShouldNotBeFound("city.specified=false");
    }
                @Test
    @Transactional
    public void getAllFinancialInstitutionsByCityContainsSomething() throws Exception {
        // Initialize the database
        financialInstitutionRepository.saveAndFlush(financialInstitution);

        // Get all the financialInstitutionList where city contains DEFAULT_CITY
        defaultFinancialInstitutionShouldBeFound("city.contains=" + DEFAULT_CITY);

        // Get all the financialInstitutionList where city contains UPDATED_CITY
        defaultFinancialInstitutionShouldNotBeFound("city.contains=" + UPDATED_CITY);
    }

    @Test
    @Transactional
    public void getAllFinancialInstitutionsByCityNotContainsSomething() throws Exception {
        // Initialize the database
        financialInstitutionRepository.saveAndFlush(financialInstitution);

        // Get all the financialInstitutionList where city does not contain DEFAULT_CITY
        defaultFinancialInstitutionShouldNotBeFound("city.doesNotContain=" + DEFAULT_CITY);

        // Get all the financialInstitutionList where city does not contain UPDATED_CITY
        defaultFinancialInstitutionShouldBeFound("city.doesNotContain=" + UPDATED_CITY);
    }


    @Test
    @Transactional
    public void getAllFinancialInstitutionsByStateIsEqualToSomething() throws Exception {
        // Initialize the database
        financialInstitutionRepository.saveAndFlush(financialInstitution);

        // Get all the financialInstitutionList where state equals to DEFAULT_STATE
        defaultFinancialInstitutionShouldBeFound("state.equals=" + DEFAULT_STATE);

        // Get all the financialInstitutionList where state equals to UPDATED_STATE
        defaultFinancialInstitutionShouldNotBeFound("state.equals=" + UPDATED_STATE);
    }

    @Test
    @Transactional
    public void getAllFinancialInstitutionsByStateIsNotEqualToSomething() throws Exception {
        // Initialize the database
        financialInstitutionRepository.saveAndFlush(financialInstitution);

        // Get all the financialInstitutionList where state not equals to DEFAULT_STATE
        defaultFinancialInstitutionShouldNotBeFound("state.notEquals=" + DEFAULT_STATE);

        // Get all the financialInstitutionList where state not equals to UPDATED_STATE
        defaultFinancialInstitutionShouldBeFound("state.notEquals=" + UPDATED_STATE);
    }

    @Test
    @Transactional
    public void getAllFinancialInstitutionsByStateIsInShouldWork() throws Exception {
        // Initialize the database
        financialInstitutionRepository.saveAndFlush(financialInstitution);

        // Get all the financialInstitutionList where state in DEFAULT_STATE or UPDATED_STATE
        defaultFinancialInstitutionShouldBeFound("state.in=" + DEFAULT_STATE + "," + UPDATED_STATE);

        // Get all the financialInstitutionList where state equals to UPDATED_STATE
        defaultFinancialInstitutionShouldNotBeFound("state.in=" + UPDATED_STATE);
    }

    @Test
    @Transactional
    public void getAllFinancialInstitutionsByStateIsNullOrNotNull() throws Exception {
        // Initialize the database
        financialInstitutionRepository.saveAndFlush(financialInstitution);

        // Get all the financialInstitutionList where state is not null
        defaultFinancialInstitutionShouldBeFound("state.specified=true");

        // Get all the financialInstitutionList where state is null
        defaultFinancialInstitutionShouldNotBeFound("state.specified=false");
    }
                @Test
    @Transactional
    public void getAllFinancialInstitutionsByStateContainsSomething() throws Exception {
        // Initialize the database
        financialInstitutionRepository.saveAndFlush(financialInstitution);

        // Get all the financialInstitutionList where state contains DEFAULT_STATE
        defaultFinancialInstitutionShouldBeFound("state.contains=" + DEFAULT_STATE);

        // Get all the financialInstitutionList where state contains UPDATED_STATE
        defaultFinancialInstitutionShouldNotBeFound("state.contains=" + UPDATED_STATE);
    }

    @Test
    @Transactional
    public void getAllFinancialInstitutionsByStateNotContainsSomething() throws Exception {
        // Initialize the database
        financialInstitutionRepository.saveAndFlush(financialInstitution);

        // Get all the financialInstitutionList where state does not contain DEFAULT_STATE
        defaultFinancialInstitutionShouldNotBeFound("state.doesNotContain=" + DEFAULT_STATE);

        // Get all the financialInstitutionList where state does not contain UPDATED_STATE
        defaultFinancialInstitutionShouldBeFound("state.doesNotContain=" + UPDATED_STATE);
    }


    @Test
    @Transactional
    public void getAllFinancialInstitutionsByPostalCodeIsEqualToSomething() throws Exception {
        // Initialize the database
        financialInstitutionRepository.saveAndFlush(financialInstitution);

        // Get all the financialInstitutionList where postalCode equals to DEFAULT_POSTAL_CODE
        defaultFinancialInstitutionShouldBeFound("postalCode.equals=" + DEFAULT_POSTAL_CODE);

        // Get all the financialInstitutionList where postalCode equals to UPDATED_POSTAL_CODE
        defaultFinancialInstitutionShouldNotBeFound("postalCode.equals=" + UPDATED_POSTAL_CODE);
    }

    @Test
    @Transactional
    public void getAllFinancialInstitutionsByPostalCodeIsNotEqualToSomething() throws Exception {
        // Initialize the database
        financialInstitutionRepository.saveAndFlush(financialInstitution);

        // Get all the financialInstitutionList where postalCode not equals to DEFAULT_POSTAL_CODE
        defaultFinancialInstitutionShouldNotBeFound("postalCode.notEquals=" + DEFAULT_POSTAL_CODE);

        // Get all the financialInstitutionList where postalCode not equals to UPDATED_POSTAL_CODE
        defaultFinancialInstitutionShouldBeFound("postalCode.notEquals=" + UPDATED_POSTAL_CODE);
    }

    @Test
    @Transactional
    public void getAllFinancialInstitutionsByPostalCodeIsInShouldWork() throws Exception {
        // Initialize the database
        financialInstitutionRepository.saveAndFlush(financialInstitution);

        // Get all the financialInstitutionList where postalCode in DEFAULT_POSTAL_CODE or UPDATED_POSTAL_CODE
        defaultFinancialInstitutionShouldBeFound("postalCode.in=" + DEFAULT_POSTAL_CODE + "," + UPDATED_POSTAL_CODE);

        // Get all the financialInstitutionList where postalCode equals to UPDATED_POSTAL_CODE
        defaultFinancialInstitutionShouldNotBeFound("postalCode.in=" + UPDATED_POSTAL_CODE);
    }

    @Test
    @Transactional
    public void getAllFinancialInstitutionsByPostalCodeIsNullOrNotNull() throws Exception {
        // Initialize the database
        financialInstitutionRepository.saveAndFlush(financialInstitution);

        // Get all the financialInstitutionList where postalCode is not null
        defaultFinancialInstitutionShouldBeFound("postalCode.specified=true");

        // Get all the financialInstitutionList where postalCode is null
        defaultFinancialInstitutionShouldNotBeFound("postalCode.specified=false");
    }
                @Test
    @Transactional
    public void getAllFinancialInstitutionsByPostalCodeContainsSomething() throws Exception {
        // Initialize the database
        financialInstitutionRepository.saveAndFlush(financialInstitution);

        // Get all the financialInstitutionList where postalCode contains DEFAULT_POSTAL_CODE
        defaultFinancialInstitutionShouldBeFound("postalCode.contains=" + DEFAULT_POSTAL_CODE);

        // Get all the financialInstitutionList where postalCode contains UPDATED_POSTAL_CODE
        defaultFinancialInstitutionShouldNotBeFound("postalCode.contains=" + UPDATED_POSTAL_CODE);
    }

    @Test
    @Transactional
    public void getAllFinancialInstitutionsByPostalCodeNotContainsSomething() throws Exception {
        // Initialize the database
        financialInstitutionRepository.saveAndFlush(financialInstitution);

        // Get all the financialInstitutionList where postalCode does not contain DEFAULT_POSTAL_CODE
        defaultFinancialInstitutionShouldNotBeFound("postalCode.doesNotContain=" + DEFAULT_POSTAL_CODE);

        // Get all the financialInstitutionList where postalCode does not contain UPDATED_POSTAL_CODE
        defaultFinancialInstitutionShouldBeFound("postalCode.doesNotContain=" + UPDATED_POSTAL_CODE);
    }


    @Test
    @Transactional
    public void getAllFinancialInstitutionsByBicCodesIsEqualToSomething() throws Exception {
        // Initialize the database
        financialInstitutionRepository.saveAndFlush(financialInstitution);
        BicCode bicCodes = BicCodeResourceIT.createEntity(em);
        em.persist(bicCodes);
        em.flush();
        financialInstitution.addBicCodes(bicCodes);
        financialInstitutionRepository.saveAndFlush(financialInstitution);
        Long bicCodesId = bicCodes.getId();

        // Get all the financialInstitutionList where bicCodes equals to bicCodesId
        defaultFinancialInstitutionShouldBeFound("bicCodesId.equals=" + bicCodesId);

        // Get all the financialInstitutionList where bicCodes equals to bicCodesId + 1
        defaultFinancialInstitutionShouldNotBeFound("bicCodesId.equals=" + (bicCodesId + 1));
    }


    @Test
    @Transactional
    public void getAllFinancialInstitutionsByCountryIsEqualToSomething() throws Exception {
        // Initialize the database
        financialInstitutionRepository.saveAndFlush(financialInstitution);
        Country country = CountryStubFactory.createEntity();
        em.persist(country);
        em.flush();
        financialInstitution.setCountry(country);
        financialInstitutionRepository.saveAndFlush(financialInstitution);
        Long countryId = country.getId();

        // Get all the financialInstitutionList where country equals to countryId
        defaultFinancialInstitutionShouldBeFound("countryId.equals=" + countryId);

        // Get all the financialInstitutionList where country equals to countryId + 1
        defaultFinancialInstitutionShouldNotBeFound("countryId.equals=" + (countryId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultFinancialInstitutionShouldBeFound(String filter) throws Exception {
        restFinancialInstitutionMockMvc.perform(get("/api/financial-institutions?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(financialInstitution.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION)))
            .andExpect(jsonPath("$.[*].address").value(hasItem(DEFAULT_ADDRESS)))
            .andExpect(jsonPath("$.[*].address2").value(hasItem(DEFAULT_ADDRESS_2)))
            .andExpect(jsonPath("$.[*].city").value(hasItem(DEFAULT_CITY)))
            .andExpect(jsonPath("$.[*].state").value(hasItem(DEFAULT_STATE)))
            .andExpect(jsonPath("$.[*].postalCode").value(hasItem(DEFAULT_POSTAL_CODE)));

        // Check, that the count call also returns 1
        restFinancialInstitutionMockMvc.perform(get("/api/financial-institutions/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultFinancialInstitutionShouldNotBeFound(String filter) throws Exception {
        restFinancialInstitutionMockMvc.perform(get("/api/financial-institutions?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restFinancialInstitutionMockMvc.perform(get("/api/financial-institutions/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    public void getNonExistingFinancialInstitution() throws Exception {
        // Get the financialInstitution
        restFinancialInstitutionMockMvc.perform(get("/api/financial-institutions/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateFinancialInstitution() throws Exception {
        // Initialize the database
        financialInstitutionService.save(financialInstitution);

        int databaseSizeBeforeUpdate = financialInstitutionRepository.findAll().size();

        // Update the financialInstitution
        FinancialInstitution updatedFinancialInstitution = financialInstitutionRepository.findById(financialInstitution.getId()).get();
        // Disconnect from session so that the updates on updatedFinancialInstitution are not directly saved in db
        em.detach(updatedFinancialInstitution);
        updatedFinancialInstitution
            .name(UPDATED_NAME)
            .description(UPDATED_DESCRIPTION)
            .address(UPDATED_ADDRESS)
            .address2(UPDATED_ADDRESS_2)
            .city(UPDATED_CITY)
            .state(UPDATED_STATE)
            .postalCode(UPDATED_POSTAL_CODE);

        restFinancialInstitutionMockMvc.perform(put("/api/financial-institutions")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedFinancialInstitution)))
            .andExpect(status().isOk());

        // Validate the FinancialInstitution in the database
        List<FinancialInstitution> financialInstitutionList = financialInstitutionRepository.findAll();
        assertThat(financialInstitutionList).hasSize(databaseSizeBeforeUpdate);
        FinancialInstitution testFinancialInstitution = financialInstitutionList.get(financialInstitutionList.size() - 1);
        assertThat(testFinancialInstitution.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testFinancialInstitution.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testFinancialInstitution.getAddress()).isEqualTo(UPDATED_ADDRESS);
        assertThat(testFinancialInstitution.getAddress2()).isEqualTo(UPDATED_ADDRESS_2);
        assertThat(testFinancialInstitution.getCity()).isEqualTo(UPDATED_CITY);
        assertThat(testFinancialInstitution.getState()).isEqualTo(UPDATED_STATE);
        assertThat(testFinancialInstitution.getPostalCode()).isEqualTo(UPDATED_POSTAL_CODE);
    }

    @Test
    @Transactional
    public void updateNonExistingFinancialInstitution() throws Exception {
        int databaseSizeBeforeUpdate = financialInstitutionRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restFinancialInstitutionMockMvc.perform(put("/api/financial-institutions")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(financialInstitution)))
            .andExpect(status().isBadRequest());

        // Validate the FinancialInstitution in the database
        List<FinancialInstitution> financialInstitutionList = financialInstitutionRepository.findAll();
        assertThat(financialInstitutionList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteFinancialInstitution() throws Exception {
        // Initialize the database
        financialInstitutionService.save(financialInstitution);

        int databaseSizeBeforeDelete = financialInstitutionRepository.findAll().size();

        // Delete the financialInstitution
        restFinancialInstitutionMockMvc.perform(delete("/api/financial-institutions/{id}", financialInstitution.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<FinancialInstitution> financialInstitutionList = financialInstitutionRepository.findAll();
        assertThat(financialInstitutionList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
